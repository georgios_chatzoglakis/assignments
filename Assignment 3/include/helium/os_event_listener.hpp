// os_event_listener.hpp

#ifndef OSEVENTLISTENER_HPP_INCLUDED
#define OSEVENTLISTENER_HPP_INCLUDED

namespace helium
{
	class OSEvent;

	class OSEventListener
	{
	public:
		virtual ~OSEventListener() {}
		virtual void notify(OSEvent*) = 0;
	};
}

#endif // OSEVENTLISTENER_HPP_INCLUDED
