//PacketManager.hpp
//By Georgios Chatzoglakis

#ifndef PACKETMANAGER_HPP_INCLUDED
#define PACKETMANAGER_HPP_INCLUDED

#include<list>

namespace goliath
{
	namespace network
	{
#define MAX_PACKET_SIZE 512
		enum ePacketType
		{
			PACKET_TYPE_INIT,
			PACKET_TYPE_PING,
			PACKET_TYPE_CLIENT_INPUT,
			PACKET_TYPE_CLIENT_POSITION,
			PACKET_TYPE_SERVER_STATE,
			PACKET_TYPE_SHUTDOWN,
		};

		class Packet;
		class PacketManager
		{
		public:

			PacketManager();
			~PacketManager();

			/*_Data = received data,
			_Flag(0 = incoming Packet)
			Returns a false if wrong flag is entered*/
			bool CreateRecvPacket(const char* _Data, const unsigned char _Flag);
			/*
			_PacketID(0 = To ALL, 1-4 = to specific client)
			_Type = PacketType
			_Data = data to send,
			_Flag(1 = outgoing Packet)
			Returns a false if wrong flag is entered*/
			bool CreateSendPacket(const unsigned char _PacketID, ePacketType _Type, const char* _Data, const unsigned char _Flag);

			const unsigned char ReturnPacketType(Packet* _ActivePacket);
			/*Only used when raw buffer data is recieved*/
			bool CheckDataIsInit(const char* _Data);
			/*Returns nullptr if the list is empty*/
			Packet* GetNextRecvPacket();
			/*Returns nullptr if the list is empty*/
			Packet* GetLastRecvPacket();
			/*Returns nullptr if the list is empty*/
			Packet* GetNextSendPacket();

			/*Returns true if packet was sucessfully removed*/
			bool RemoveFromRecvList();
			/*Returns true if packet was sucessfully removed*/
			bool RemoveFromSendList();

		private:
			void AddToRecvList(Packet* _NewPacket);
			void AddToSendList(Packet* _NewPacket);



			std::list<Packet*> m_RecvList;
			std::list<Packet*> m_SendList;
		};
	}
}
#endif //PACKETMANAGER_HPP_INCLUDED