// material.hpp

#ifndef MATERIAL_HPP_INCLUDED
#define MATERIAL_HPP_INCLUDED

namespace helium
{
	namespace resource
	{
		class Material 
		{
		private: // non-copyable
			Material(const Material&);
			Material& operator=(const Material&);

		public:
			typedef std::unique_ptr<Material> Ptr;

			static Ptr create(const std::string& filename);

		private:
			Material();

		public:
			~Material();

			bool has_property(const std::string& key) const;
			const std::string& get_property(const std::string& key) const;

		private:
			std::unordered_map<std::string, std::string> m_properties;
		};
	}
}

#endif // MATERIAL_HPP_INCLUDED
