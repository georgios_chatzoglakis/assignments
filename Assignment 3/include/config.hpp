// config.hpp

#ifndef NDEBUG
#	define HELIUM_DEBUG
#	define GOLIATH_DEBUG
#else
#	define HELIUM_RELEASE
#	define GOLIATH_RELEASE
#endif
