// os_event.cpp

#include "stdafx.h"
#include <helium/os_event.hpp>

namespace helium
{
	OSEvent::OSEvent()
	{
		m_type = EOSEventType::Invalid;
	}

	OSEvent::OSEvent(EOSEventType type)
	{
		m_type = type;
	}
}
