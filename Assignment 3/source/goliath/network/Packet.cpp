//Packet.cpp
//By Georgios Chatzoglakis

#include <goliath/network/Packet.hpp>

#include <cstring>
#include <iostream>
namespace goliath
{
	namespace network
	{

		/*INCOMING PACKET,
		_IncomeBuffer(Raw Data Received from Socket)
		_Flag( 0 = incoming)*/
		Packet::Packet(const char* _IncomeBuffer, const unsigned char _Flag)
		{
			m_PacketBuff = nullptr;
			m_Data = nullptr;
			m_Header = nullptr;

			ReadPacketBuffer(_IncomeBuffer);
			//change flag to incoming since the data is now recieved.
			m_Header->s_Flag = _Flag;

			std::cout << "New Flag:" << m_Header->s_Flag << std::endl;
		}
		/*OUTGOING PACKET,
		_PacketID, (0 = Server, 1-4 = Client/Player number.)
		_Type, (what type of packet.)
		_Timestamp,(when packet was created.)
		_Data,(actual data)
		_Flag(1 = outgoing)*/
		Packet::Packet(const unsigned char _PacketID, unsigned char _Type, const unsigned int _Timestamp,
			const char* _Data, const unsigned char _Flag)
		{
			m_PacketBuff = nullptr;
			m_Data = nullptr;
			m_Header = new PacketHeader;

			m_Header->s_PacketID = _PacketID;
			m_Header->s_Type = _Type;
			m_Header->s_Timestamp = _Timestamp;
			m_Header->s_HeaderSize = sizeof(struct PacketHeader);
			m_Header->s_Flag = _Flag;
			m_Data = _Data;

			CreatePacketBuffer();
		};
		Packet::~Packet()
		{
			if (m_Header != nullptr)
			{
				delete m_Header;
				m_Header = nullptr;
			}
		}
		const unsigned char& Packet::GetID()
		{
			return m_Header->s_PacketID;
		};
		void Packet::SetID(const unsigned char _NewID)
		{
			m_Header->s_PacketID = _NewID;
		}
		const unsigned char& Packet::GetType()
		{
			return m_Header->s_Type;
		};
		const unsigned int& Packet::GetTimeStamp()
		{
			return m_Header->s_Timestamp;
		};
		const unsigned short& Packet::GetHeaderSize()
		{
			return m_Header->s_HeaderSize;
		};

		Packet::PacketHeader* Packet::GetPacketHeader()
		{
			return m_Header;
		};
		const char* Packet::GetData()
		{
			return m_Data;
		};
		const char* Packet::GetPacketBuffer()
		{
			return m_PacketBuff;
		};
		bool Packet::ReadPacketBuffer(const char* _IncomeBuffer)
		{
			memcpy(&m_Header, _IncomeBuffer, sizeof(struct PacketHeader));
			m_Data = _IncomeBuffer + m_Header->s_HeaderSize;

			std::cout << "PacketID:" << m_Header->s_PacketID << std::endl;
			std::cout << "Headersize:" << m_Header->s_HeaderSize << std::endl;
			std::cout << "PacketType:" << m_Header->s_Type << std::endl;
			std::cout << "Timestamp:" << m_Header->s_Timestamp << std::endl;
			std::cout << "Flag:" << m_Header->s_Flag << std::endl;
			if (m_Header != nullptr && m_Data != nullptr)
				return true;

			std::cout << "Error: Packet Could not be read!" << std::endl;
			return false;
		}
		bool Packet::CreatePacketBuffer()
		{
			memcpy(m_PacketBuff, m_Header, sizeof(struct PacketHeader));
			memset(m_PacketBuff + m_Header->s_HeaderSize, (int)m_Data, sizeof(m_Data));
			if (m_PacketBuff != nullptr)
				return true;
			return false;
		}
	}
}