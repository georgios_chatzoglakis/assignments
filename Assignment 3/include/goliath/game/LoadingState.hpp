//LoadingState.hpp

#ifndef LOADINGSTATE_HPP_INCLUDED
#define LOADINGSTATE_HPP_INCLUDED

#include <helium/gameplay/abstract_state.hpp>
namespace helium
{
	namespace resource
	{
		class Font;
	}
}
namespace goliath
{
	namespace game
	{
		class LoadingState : public helium::gameplay::AbstractState
		{
		public:
			LoadingState();
			~LoadingState();

			void initialize();
			void shutdown();
			bool update();
			void draw();
			uint32_t next();

		private:
			helium::resource::Font* m_font;
			uint32_t m_vertex_buffer;
			uint32_t m_index_buffer;
			std::list<std::string> m_resources;

			struct Vertex
			{
				helium::Vector3 position;
				helium::Vector2 texcoord;
			};

			Vertex* m_vertices;
		};
	}
}

#endif //LOADINGSTATE_HPP_INCLUDED
