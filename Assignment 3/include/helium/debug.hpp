// debug.hpp

#ifndef DEBUG_HPP_INCLUDED
#define DEBUG_HPP_INCLUDED

namespace helium
{
	enum class EDebugLevel : uint32_t
	{
		Verbose,
		Info,
		Warning,
		Error,
	};

	class Debug
	{
		static EDebugLevel ms_level;
	public:
		static void set_debug_level(EDebugLevel level);
		static void write(const EDebugLevel level, const std::string& message);

		static void fatal(const std::string& message);

	public:
		Debug();
		~Debug();

	private:
		std::ofstream m_stream;
		std::streambuf* m_old;
		HANDLE m_out;
	};
}

#endif // DEBUG_HPP_INCLUDED
