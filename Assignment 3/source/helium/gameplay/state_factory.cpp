// state_factory.hpp

#include "stdafx.h"
#include <helium/gameplay/state_factory.hpp>

namespace helium
{
	namespace gameplay
	{
		// static 
		StateFactory::Ptr StateFactory::create()
		{
			return StateFactory::Ptr(new StateFactory);
		}

		// private
		StateFactory::StateFactory()
		{
		}

		// public
		StateFactory::~StateFactory()
		{
			auto it = m_creators.begin();
			while (it != m_creators.end())
			{
				delete it->second;
				++it;
			}
			m_creators.clear();
		}

		AbstractState* StateFactory::construct(uint32_t id_hash)
		{
			auto it = m_creators.find(id_hash);
			if (it == m_creators.end())
			{
				Debug::write(EDebugLevel::Warning, "statefactory: could not find state");
				return nullptr;
			}
			return it->second->create();
		}
	}
}