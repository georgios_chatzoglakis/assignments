// render_dispatcher.cpp

#include "stdafx.h"
#include <helium/system/render_system.hpp>
#include <helium/renderer/render_queue.hpp>
#include <helium/renderer/render_dispatcher.hpp>

namespace helium
{
	namespace renderer
	{
		// static
		RenderDispatcher::Ptr RenderDispatcher::create(system::RenderSystem* render_system)
		{
			return RenderDispatcher::Ptr(new RenderDispatcher(render_system));
		}

		// private
		RenderDispatcher::RenderDispatcher(system::RenderSystem* render_system)
		{
			m_render_system = render_system;
		}

		// public
		RenderDispatcher::~RenderDispatcher()
		{
		}

		void RenderDispatcher::add_queue(RenderQueue* queue)
		{
			m_queue_list.push_back(queue);
		}

		void RenderDispatcher::process()
		{
			auto it = m_queue_list.begin();
			while (it != m_queue_list.end())
			{
				RenderCommand cmd;
				while ((*it)->pop(cmd))
				{
					switch (cmd.m_type)
					{
					case ERenderCommandType::Clear:
						m_render_system->clear();
						break;
					case ERenderCommandType::Present:
						m_render_system->present();
						break;
					case ERenderCommandType::Shader:
						m_render_system->select_shader(cmd.m_cmd.m_id);
						break;
					case ERenderCommandType::Texture:
						m_render_system->select_texture(cmd.m_cmd.m_texture.m_id, 
							cmd.m_cmd.m_texture.m_slot);
						break;
					case ERenderCommandType::VertexFormat: 
						m_render_system->select_vertex_format(cmd.m_cmd.m_id);
						break;
					case ERenderCommandType::VertexBuffer: 
						m_render_system->select_vertex_buffer(cmd.m_cmd.m_id);
						break;
					case ERenderCommandType::IndexBuffer: 
						m_render_system->select_index_buffer(cmd.m_cmd.m_id);
						break;
					case ERenderCommandType::Sampler:
						m_render_system->select_sampler_state(cmd.m_cmd.m_sampler.m_id,
							cmd.m_cmd.m_sampler.m_slot);
						break;
					case ERenderCommandType::Blend: 
						m_render_system->select_blend_state(cmd.m_cmd.m_id);
						break;
					case ERenderCommandType::Depth: 
						m_render_system->select_depth_state(cmd.m_cmd.m_id);
						break;
					case ERenderCommandType::Rasterizer:
						m_render_system->select_rasterizer_state(cmd.m_cmd.m_id);
						break;
					case ERenderCommandType::ShaderConstant: 
						m_render_system->set_shader_constant_raw(cmd.m_cmd.m_constant.m_hash,
							cmd.m_cmd.m_constant.m_data, cmd.m_cmd.m_constant.m_size);
						break;
					case ERenderCommandType::Draw: 
						m_render_system->apply();
						m_render_system->draw((system::EDrawMode)cmd.m_cmd.m_draw.m_mode,
							cmd.m_cmd.m_draw.m_start, cmd.m_cmd.m_draw.m_count);
						break;
					case ERenderCommandType::DrawIndexed: 
						m_render_system->apply();
						m_render_system->drawi((system::EDrawMode)cmd.m_cmd.m_draw.m_mode,
							cmd.m_cmd.m_draw.m_start, cmd.m_cmd.m_draw.m_count);
						break;
					}
				}
				++it;
			}
			m_queue_list.clear();
		}
	}
}
