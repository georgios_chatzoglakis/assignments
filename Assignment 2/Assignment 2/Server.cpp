//Server.cpp

#include <iostream>
#include <fstream>

#include"Server.h"

Server::Server()
{
	m_BuffLength = sizeof(m_RecvBuff);
	m_ImageData = nullptr;
	m_OutBuffer = nullptr;
	m_ImageLength = 0;
}
Server::~Server()
{
	//just in case there is clients left when the server is destroyed
	for (unsigned int i = 0; i < ClientList.size(); i++)
	{
		delete ClientList[i];
		ClientList[i] = nullptr;
		ClientList.erase(ClientList.begin() + i);
		--i;
	}
	if (m_ImageData != nullptr)
		delete[] m_ImageData;
	if (m_OutBuffer != nullptr)
		delete m_OutBuffer;
}
bool Server::InitializeWinsock()
{
	if (WSAStartup(MAKEWORD(2, 2), &m_Data))
	{
		std::cout << "Failed to initialize Winsock" << std::endl;
		return false;
	}
	return true;
}

void Server::CleanupWinsock()
{
	WSACleanup();
}

SOCKET Server::socketCreate()
{
	return socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

void Server::socketDestroy(SOCKET& _sock)
{
	closesocket(_sock);
}

void Server::setupLocalAndPort()
{
	m_Local.sin_family = AF_INET;
	//127.0.0.1
	//m_Local.sin_addr.S_un.S_un_b.s_b1 = 127;
	//m_Local.sin_addr.S_un.S_un_b.s_b2 = 0;
	//m_Local.sin_addr.S_un.S_un_b.s_b3 = 0;
	//m_Local.sin_addr.S_un.S_un_b.s_b4 = 1;
	m_Local.sin_addr.S_un.S_addr = INADDR_ANY;
	m_Local.sin_port = htons(8080);
	memset(m_Local.sin_zero, 0, sizeof(m_Local.sin_zero));
}

bool Server::bindSocket(SOCKET& _ListenSock)
{
	if (bind(_ListenSock, (const sockaddr*)&m_Local, sizeof(m_Local)) < 0)
	{
		std::cout << "Could not bind socket" << std::endl;
		std::cout << "Error: " << WSAGetLastError() << std::endl;
		return false;
	}
	return true;
}

bool Server::listenOnSocket(SOCKET& _ListenSock)
{
	if (listen(_ListenSock, SOMAXCONN) == SOCKET_ERROR)		//SOMAXCONN == reasonable maximum of socket connections
	{
		std::cout << "Listening failed, Error: " << WSAGetLastError();
		return false;
	}
	return true;
}

bool Server::acceptClient(SOCKET _ListenSock)
{
	// Creates a socket that will be used to accept a client
	SOCKET clientSock = INVALID_SOCKET;
	struct sockaddr_in Addr = { 0 };
	int size = sizeof(Addr);

	//kod fr�n Tommi nedan f�r att sl�ppa igenom en connection �t g�ngen
	fd_set fd;
	fd.fd_count = 1;
	fd.fd_array[0] = _ListenSock;
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 10;
	if (select(1, &fd, 0, 0, &tv) > 0)
		//kod fr�n Tommi ovan.
	{
		//std::cout << "About to accept()" << std::endl;
		clientSock = accept(_ListenSock, (struct sockaddr*)&Addr, &size);
		if (clientSock == INVALID_SOCKET)
		{
			std::cout << "Accept client failed, Error: " << WSAGetLastError() << std::endl;
			return false;
		}
		else
		{
			addClient(clientSock, Addr);
		}
		return true;
	}
	//std::cout << "Skipped Accept()" << std::endl;
	return false;
}
void Server::addClient(SOCKET _ClientSock, struct sockaddr_in& _Addr)
{
	//Add a new client
	Client* client = new Client;
	client->s_Sock = _ClientSock;
	client->s_Addr = _Addr;
	client->s_Timestamp = timeGetTime();
	client->s_Tick = 0;
	//keep clients alive by default
	client->KeepAlive = true;
	ClientList.push_back(client);
	std::cout << "New connection: ";
	printClientAddress(&client->s_Addr);
}

void Server::UpdateClientConnections()
{
	for (unsigned int i = 0; i < ClientList.size(); i++)
	{
		//does any of active conncetions have data
		if (clientHasData(ClientList[i]) > 0)
		{
			//std::cout << "Has data" << std::endl;
			//Handle data
			serverHandleData(ClientList[i]);
		}
		//keep alive
		if (!clientCheckAlive(ClientList[i], i))
		{
			--i;
		}
	}
	//std::cout << "Number of active connections: " << ClientList.size() << std::endl;
}

int Server::clientHasData(Client* _Client)
{
	u_long len = 0;
	ioctlsocket(_Client->s_Sock, FIONREAD, &len);
	if (len < 0)
	{
		//std::cout << "has no data" << std::endl;
	}
	else if (len == 0)
	{
		//std::cout << "Half open connection" << std::endl;
	}
	return len;
}

int Server::serverHandleData(Client* _Client)
{
	struct sockaddr_in remote;
	int size = sizeof(remote);

	int recvBytes = recvfrom(_Client->s_Sock, m_RecvBuff, m_BuffLength - 1, 0, (struct sockaddr*)&remote, &size);
	if (recvBytes == SOCKET_ERROR)
	{
		std::cout << "Error recieving data: " << WSAGetLastError();
	}
	else if (recvBytes > 0)
	{
		m_RecvBuff[recvBytes] = '\0';
		//std::cout << recvBytes << "Bytes recived!" << std::endl;
		//std::cout << m_RecvBuff << std::endl;

		//Update Timestamp if data is recieved
		clientUpdateTimestamp(_Client);

		//below I used some of the logic from C# example http://www.codeproject.com/Articles/452052/Build-Your-Own-Web-Server#get
		std::string StrBuff = m_RecvBuff;
		std::string httpMethod = "";

		//Get the substring between GET and HTTP
		int start = StrBuff.find("GET ") + sizeof("GET");
		int length = StrBuff.find(" HTTP") - start;
		std::string subStr = StrBuff.substr(start, length);

		//std::cout << "Substring: " << subStr << std::endl;

		//Check Request
		if (subStr.find("/") != std::string::npos)
		{
			if (subStr == "/")
			{
				httpMethod = "index.html";
			}
			//look for invalid GET request made with dots /../.
			//look for first dot in substring
			else if (subStr.find(".") != std::string::npos)
			{
				//look for a second dot after the first one
				if (subStr.find(".", (subStr.find(".") + 1)) != std::string::npos)
					httpMethod = "404.html";
				else
					httpMethod = subStr;
			}
			//if it's not index page or an invalid request just send the whole substring
			else
			{
				//std::cout << "Substring: " << subStr << std::endl;
				httpMethod = subStr;
			}

		}

		if (StrBuff.find("Connection: ") != std::string::npos)
		{
			if (StrBuff.find("Keep-Alive", (StrBuff.find("Connection:") + 1)) != std::string::npos)
				_Client->KeepAlive = true;
			else if (StrBuff.find("keep-alive", (StrBuff.find("Connection:") + 1)) != std::string::npos)
				_Client->KeepAlive = true;
			else if (StrBuff.find("close", (StrBuff.find("Connection:") + 1)) != std::string::npos)
				_Client->KeepAlive = false;
			else if (StrBuff.find("Close", (StrBuff.find("Connection:") + 1)) != std::string::npos)
				_Client->KeepAlive = false;
		}
		//send data	
		serverSendData(_Client, httpMethod);

	}
	return 0;
}

void Server::clientUpdateTimestamp(Client* _Client)
{
	_Client->s_Timestamp = timeGetTime();
	//std::cout << "Client: ";
	//printClientAddress(&_Client->s_Addr);
	//std::cout << " is still alive." << std::endl;
}

bool Server::clientCheckAlive(Client* _Client, unsigned int& _Pos)
{
	unsigned int now = timeGetTime();
	if (_Client->KeepAlive)
	{
		if (now - _Client->s_Timestamp > 300000)		//300.000 == 5 minutes
		{
			std::cout << "Timeout: ";
			printClientAddress(&_Client->s_Addr);
			removeClient(_Pos);
			return false;
		}
	}
	else
	{
		std::cout << "Not a Keep-Alive connection, Closing: ";
		printClientAddress(&_Client->s_Addr);
		removeClient(_Pos);
		return false;
	}
	return true;
}

bool Server::serverSendData(Client* _Client, const std::string _Type)
{
	//std::string SendStr = "";

	m_Html = "";
	//Send header + webpage

	if (loadWebPage(_Type) && (_Type.compare("404.html") != 0) && (_Type.compare("/GeoWeb.jpg") != 0))
	{
		m_Headerinfo.s_HttpStatus = "200 OK";
	}
	else if (_Type.compare("/GeoWeb.jpg") == 0)
	{
		if (m_ImageData = loadImage("GeoWeb.jpg"))
		{
			m_Headerinfo.s_ContentLength = m_ImageLength;
			updateHeader();
			int bufferLength = m_HeaderStr.length() + m_ImageLength;
			char *m_OutBuffer = new char[bufferLength];

			for (unsigned int i = 0; i < m_HeaderStr.length(); i++)
			{
				m_OutBuffer[i] = m_HeaderStr[i];
			}
			for (unsigned int i = m_HeaderStr.length(); i - m_HeaderStr.length() < m_ImageLength; i++)
			{
				m_OutBuffer[i] = m_ImageData[i - m_HeaderStr.length()];
			}
			int BytesSent = send(_Client->s_Sock, m_OutBuffer, bufferLength, 0);
			
			if (BytesSent == SOCKET_ERROR)
			{
				std::cout << "Failed to send html data, Error: " << WSAGetLastError();
				delete[] m_ImageData;
				m_ImageData = nullptr;
				delete[] m_OutBuffer;
				m_OutBuffer = nullptr;
				return false;
			}
			delete[] m_ImageData;
			m_ImageData = nullptr;
			delete[] m_OutBuffer;
			m_OutBuffer = nullptr;
			return true;
		}
		else
		{
			delete[] m_ImageData;
			m_ImageData = nullptr;
			return false;
		}
	}
	else
	{
		loadWebPage("404.html");
		m_Headerinfo.s_HttpStatus = "404 Not Found";
	}

	m_Headerinfo.s_ContentLength = m_Html.length();
	updateHeader();
	int bufferLength = m_HeaderStr.length() + m_Html.length();
	char *m_OutBuffer = new char[bufferLength +1];
	
	for (unsigned int i = 0; i < m_HeaderStr.length(); i++)
	{
		m_OutBuffer[i] = m_HeaderStr[i];
	}
	for (unsigned int i = m_HeaderStr.length(); i - m_HeaderStr.length() < m_Html.length(); i++)
	{
		m_OutBuffer[i] = m_Html[i - m_HeaderStr.length()];
	}
	m_OutBuffer[bufferLength] = '\0';
	//std::cout << m_Html;
	//std::cout << m_OutBuffer;

	int BytesSent = send(_Client->s_Sock, m_OutBuffer, bufferLength + 1, 0);
	if (BytesSent == SOCKET_ERROR)
	{
		std::cout << "Failed to send html data, Error: " << WSAGetLastError();
		delete [] m_OutBuffer;
		m_OutBuffer = nullptr;
		return false;
	}
	//std::cout << "Bytes sent: " << BytesSent << std::endl;
	delete[] m_OutBuffer;
	m_OutBuffer = nullptr;
	return true;

}

void Server::removeClient(unsigned int& _Pos)
{
	shutdownConnection(ClientList[_Pos]->s_Sock);
	socketDestroy(ClientList[_Pos]->s_Sock);
	delete ClientList[_Pos];
	ClientList[_Pos] = nullptr;
	ClientList.erase(ClientList.begin() + _Pos);
}

bool Server::shutdownConnection(SOCKET& _ClientSock)
{
	if (shutdown(_ClientSock, SD_SEND) == SOCKET_ERROR)		//shuts down client connection.
	{
		std::cout << "Failed to shut down client correctly, Error: " << WSAGetLastError();
		return false;
	}
	return true;
}

void Server::printClientAddress(struct sockaddr_in* _Addr)
{
	std::cout << (int)_Addr->sin_addr.S_un.S_un_b.s_b1 << ".";
	std::cout << (int)_Addr->sin_addr.S_un.S_un_b.s_b2 << ".";
	std::cout << (int)_Addr->sin_addr.S_un.S_un_b.s_b3 << ".";
	std::cout << (int)_Addr->sin_addr.S_un.S_un_b.s_b4 << std::endl;
}

bool Server::loadWebPage(const std::string& _WebAddr)
{
	std::string Path = "Webpage/" + _WebAddr;
	m_Html = "";
	std::fstream stream;

	stream.open(Path);
	if (!stream.is_open())
	{
		std::cout << "Error opening file" << std::endl;
		return false;
	}

	std::string line;
	while (!stream.eof())
	{
		std::getline(stream, line);
		m_Html += line;
	}
	stream.close();
	return true;
}
char* Server::loadImage(const std::string& _ImgName)
{
	std::string Path = "Webpage/" + _ImgName;
	std::ifstream stream;

	stream.open(Path, std::ios::in | std::ios::binary);

	stream.seekg(0, std::ios::end);
	m_ImageLength = (int)stream.tellg();
	char* m_Imagebuff = new char[m_ImageLength];

	if (!stream.is_open())
	{
		std::cout << "Error opening file" << std::endl;
		return nullptr;
	}
	stream.seekg(0, std::ios::beg);
	stream.read(m_Imagebuff, sizeof(char) * m_ImageLength);

	stream.close();

	return m_Imagebuff;
}
void Server::setHeaderInfo()
{
	m_Headerinfo.s_Connection = "Keep-Alive";
	m_Headerinfo.s_ContentType = "text/html; image/jpeg";
	m_Headerinfo.s_CharSet = "utf - 8";
	m_Headerinfo.s_SeverName = "GeoServer v0.2";
}

void Server::updateHeader()
{
	setHeaderInfo();
	//creates header string
	m_HeaderStr = "";
	m_HeaderStr += "HTTP/1.1 " + m_Headerinfo.s_HttpStatus + "\n";
	m_HeaderStr += "Content-Type: " + m_Headerinfo.s_ContentType + "; " + m_Headerinfo.s_CharSet + "\n";
	m_HeaderStr += "Content-length: " + std::to_string(m_Headerinfo.s_ContentLength) + "\n";
	m_HeaderStr += "Conncection: " + m_Headerinfo.s_Connection + "\n";
	m_HeaderStr += "Server: " + m_Headerinfo.s_SeverName + "\n";
	m_HeaderStr += "\n";
}