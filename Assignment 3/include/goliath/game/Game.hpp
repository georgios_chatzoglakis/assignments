//Game.hpp

#ifndef GAME_HPP_INCLUDED
#define GAME_HPP_INCLUDED

#include <helium/gameplay/abstract_game.hpp>

namespace goliath
{
	namespace game
	{
		class Game : public helium::gameplay::AbstractGame
		{
		private: // non-copyable
			Game(const Game&);
			Game& operator=(const Game&);

		public:
			typedef std::unique_ptr<Game> Ptr;

			static helium::gameplay::AbstractGame::Ptr create();

		private:
			Game();

		public:
			~Game();
			virtual void initialize();
			virtual void shutdown();
		};
	}
}

#endif //GAME_HPP_INCLUDED
