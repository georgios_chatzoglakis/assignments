// material.cpp

#include "stdafx.h"
#include <sstream>
#include "jsmn.h"
#include <helium/resource/material.hpp>

namespace helium
{
	namespace resource
	{
		static float to_float(const char* src, jsmntok_t* tok)
		{
			char value[16] = { 0 };
			strncpy(value, src + tok->start, tok->end - tok->start);
			return (float)atof(value);
		}

		// static 
		Material::Ptr Material::create(const std::string& filename)
		{
			std::ifstream stream;
			stream.open(filename.c_str());
			if (!stream.is_open())
			{
				Debug::write(EDebugLevel::Error, String::format("material: could not find file %s", filename.c_str()));
				return Material::Ptr(nullptr);
			}

			std::unordered_map<std::string, std::string> properties;
			std::stringstream ss;
			ss << stream.rdbuf();
			stream.close();
			while (!ss.eof())
			{
				std::string key, value;
				ss >> key;
				if (key.length() == 0)
					continue;
				ss >> value;
				properties.insert(std::pair<std::string, std::string>(key, value));
			}

			Debug::write(EDebugLevel::Info, String::format("material: %s", filename.c_str()));

			Material* material = new Material;
			material->m_properties = properties;
			return Material::Ptr(material);
		}

		// private
		Material::Material()
		{
		}

		// public
		Material::~Material()
		{
			m_properties.clear();
		}

		bool Material::has_property(const std::string& key) const
		{
			return m_properties.find(key) != m_properties.end();
		}

		const std::string& Material::get_property(const std::string& key) const
		{
			auto it = m_properties.find(key);
			return it->second;
		}
	}
}