//Packet.hpp
//By Georgios Chatzoglakis

#ifndef PACKET_HPP_INCLUDED
#define PACKET_HPP_INCLUDED

namespace goliath
{
	namespace network
	{
		class Packet
		{
		public:
#pragma pack(push, 1)
			struct PacketHeader
			{
				//s_PacketID( -1 = NoName, 0 = Server, 1-4 = Clients)
				unsigned char s_PacketID;
				//Type, Init, Ping, Input data. server data etc
				unsigned char s_Type;
				//Time when Packet was created
				unsigned int s_Timestamp;
				//Headersize in bytes
				unsigned short s_HeaderSize;
				//s_Flag(0 = incoming, 1 = outgoing)
				unsigned char s_Flag;
			};
#pragma pack(pop)

			/*INCOMING PACKET,
			_IncomeBuffer(Raw Data Received from Socket)
			_Flag( 0 = incoming)*/
			Packet(const char* _IncomeBuffer, const unsigned char _Flag);
			/*OUTGOING PACKET,
			_PacketID, (0 = Server, 1-4 = Client/Player number.)
			_Type, (what type of packet.)
			_Timestamp,(when packet was created.)
			_Data,(actual data)
			_Flag(1 = outgoing)*/
			Packet(
				const unsigned char _PacketID,
				unsigned char _Type,
				const unsigned int _Timestamp,
				const char* _Data,
				const unsigned char _Flag
				);

			~Packet();

			const unsigned char& GetID();
			void SetID(const unsigned char _NewID);
			const unsigned char& GetType();
			const unsigned int& GetTimeStamp();
			const unsigned short& GetHeaderSize();

			PacketHeader* GetPacketHeader();
			const char* GetData();
			const char* GetPacketBuffer();

		private:
			bool ReadPacketBuffer(const char* _IncomeBuffer);
			bool CreatePacketBuffer();

		private:
			PacketHeader* m_Header;
			const char* m_Data;
			char* m_PacketBuff;
		};
	}
}
#endif // PACKET_HPP_INCLUDED