//main.cpp

#include <goliath/server/GameServer.hpp>

#include <iostream>
using namespace goliath;
using namespace network;

void Pause()
{
	std::cout << "Press any key to continue.";
	std::cin.get();
}

int main()
{

	GameServer Server;

	if (!Server.Initialize())
	{
		return 0;
	}
	if (Server.LookForConnections())
	{
		Server.Run();
	}
	Server.Cleanup();

	Pause();
	return 0;
}