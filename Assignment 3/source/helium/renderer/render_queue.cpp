// render_queue.cpp

#include "stdafx.h"
#include <helium/renderer/render_queue.hpp>

namespace helium
{
	namespace renderer
	{
		RenderQueue::RenderQueue()
		{
			reset();
		}

		void RenderQueue::reset()
		{
			m_index = 0;
			m_count = 0;
		}

		void RenderQueue::push(RenderCommand& cmd)
		{
			m_queue[m_index++] = cmd;
		}

		bool RenderQueue::pop(RenderCommand& cmd)
		{
			cmd = m_queue[m_count++];
			return m_count != m_index;
		}
	}
}
