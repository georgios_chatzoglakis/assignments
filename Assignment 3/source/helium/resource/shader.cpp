// shader.cpp

#include "stdafx.h"
#include <sstream>
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <helium/resource/shader.hpp>

namespace helium
{
	namespace resource
	{
		//static 
		Shader::Ptr Shader::create(const std::string& filename)
		{
			std::ifstream stream;
			stream.open(filename.c_str());
			if (!stream.is_open())
			{
				Debug::write(EDebugLevel::Error, String::format("shader: could not find file %s", filename.c_str()));
				return Shader::Ptr(nullptr);
			}

			std::stringstream ss;
			ss << stream.rdbuf();
			stream.close();

			std::string source = ss.str();

			// todo: fix me beatiful
			char* hdrsrc = strstr(const_cast<char*>(source.c_str()), "[header]");
			char* vshsrc = strstr(const_cast<char*>(source.c_str()), "[vertex]");
			char* pshsrc = strstr(const_cast<char*>(source.c_str()), "[pixel]");
			
			if (hdrsrc)
				hdrsrc += 8;

			// pre vertex fix
			char* vsh = vshsrc;
			while (*vsh != '\n')
			{
				*vsh = '\0';
				vsh--;
			}
			vshsrc += 8;

			// pre pixel fix
			char* psh = pshsrc;
			while (*psh != '\n')
			{
				*psh = '\0';
				psh--;
			}
			pshsrc += 7;

			std::string vertex = (hdrsrc ? std::string(hdrsrc) : "") + std::string(vshsrc);
			std::string pixel = (hdrsrc ? std::string(hdrsrc) : "") + std::string(pshsrc);

			Debug::write(EDebugLevel::Info, String::format("shader: %s", filename.c_str()));

			return Shader::Ptr(new Shader(vertex, pixel));
		}

		// private
		Shader::Shader(const std::string& vertex, const std::string& pixel)
			: m_vertex(vertex)
			, m_pixel(pixel)
		{
		}

		// public
		Shader::~Shader()
		{
			m_vertex.clear();
			m_pixel.clear();
		}

		bool Shader::is_valid() const
		{
			return m_vertex.length()> 0 && m_pixel.length() > 0;
		}

		const std::string& Shader::get_vertex_source() const
		{
			return m_vertex;
		}

		const std::string& Shader::get_pixel_source() const
		{
			return m_pixel;
		}

		void Shader::dispose()
		{
			if (m_id == (uint32_t)-1)
				return;
			ServiceLocator<system::RenderSystem>::get_service()->destroy_shader(m_id);
		}
	}
}
