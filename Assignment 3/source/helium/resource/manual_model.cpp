// manual_model.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <helium/resource/manual_model.hpp>

namespace helium
{
	namespace resource
	{
		// static 
		ManualModel::Ptr ManualModel::create()
		{
			return ManualModel::Ptr(new ManualModel);
		}

		// private
		ManualModel::ManualModel()
		{
		}

		// public
		ManualModel::~ManualModel()
		{
		}
	}
}
