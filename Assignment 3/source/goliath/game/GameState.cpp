// GameState.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/resource/resource_cache.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/scene/camera.hpp>
#include <helium/scene/transform.hpp>
#include <helium/scene/lighting_property.hpp>
#include <helium/scene/shader_unit.hpp>
#include <helium/scene/shader_parameter.hpp>
#include <helium/scene/texture_unit.hpp>
#include <helium/scene/technique.hpp>
#include <helium/scene/technique_factory.hpp>
#include <goliath/game/GameState.hpp>

namespace goliath
{
	namespace game
	{
		GameState::GameState()
		{
		}

		GameState::~GameState()
		{
		}

		void GameState::initialize()
		{
			// create scene
			m_scene = helium::scene::Scene::create();
			helium::scene::SceneNode* node = m_scene->create_scene_node();
			(void)node;
		}

		void GameState::shutdown()
		{
		}

		bool GameState::update()
		{
			return true;
		}

		void GameState::draw()
		{
		}

		uint32_t GameState::next()
		{
			return 0;
		}
	}
}
