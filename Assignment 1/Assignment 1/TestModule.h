//TestModule.h
//This is a TestModule to test my Linked List and Binary Search Tree
//By Georgios Chatzoglakis


#pragma once

#include<string>

template <typename T>
class LinkedList;

template <typename T>
class TestModule
{
public:
	TestModule();
	~TestModule();

	//Returns true if the variables match
	bool VerifyEqual(T _Expected, T _Got, const std::string& _Message);
	//Returns true if the variables don't match
	bool VerifyNotEqual(T _Expected, T _Got, const std::string& _Message);
	//Returns true if the Linked List doesn't have any nodes in it.
	bool VerifyListErase(LinkedList<T> _List, const std::string& _Message);
private:
	bool AreEqual(T _A, T _B);

};

template <typename T>
TestModule<T>::TestModule()
{

}

template <typename T>
TestModule<T>::~TestModule()
{

}

template <typename T>
bool TestModule<T>::VerifyEqual(T _Expected, T _Got, const std::string& _Message)
{
	if (AreEqual(_Expected, _Got))
	{
		std::cout << "Test Passed: " << _Message << std::endl;
		return true;
	}

	std::cout << _Message << " Test Failed!, Expected: " << _Expected << " Got: " << _Got << std::endl;
	return false;
}

template <typename T>
bool TestModule<T>::VerifyNotEqual(T _Expected, T _Got, const std::string& _Message)
{
	if (!AreEqual(_Expected, _Got))
	{
		std::cout << "Test Passed: " << _Message << std::endl;
		return true;
	}

	std::cout << _Message << " Test Failed!, Expected: " << _Expected << " Got: " << _Got << std::endl;
	return false;
}
template <typename T>
bool TestModule<T>::VerifyListErase(LinkedList<T> _List, const std::string &_Message)
{
	if (_List.GetFirst() == nullptr)
	{
		std::cout << "Test Passed: " << _Message << std::endl;
		return true;
	}
	else
	{
		std::cout << _Message << " Test Failed!, The list is not empty" << std::endl;
		return false;
	}
}

template <typename T>
bool TestModule<T>::AreEqual(T _A, T _B)
{
	return _A == _B;
}