//Assignment 1, including Linked list, Binary search tree and a Test module

#include <iostream>
#include <vector>

#include "BST.h"
#include "LinkedList.h"
#include "TestModule.h"
//#include "vld.h"

void ConsolePause()
{
	std::cout << std::endl << "Press ENTER to continue...";
	std::cin.clear();
	std::cin.sync();
	std::cin.ignore(std::numeric_limits <std::streamsize> ::max(), '\n');
}

int main(int argc, char* argv[])
{
	TestModule<int> Test;		//TestModule. testing int

	//Creating a list for integers;
	LinkedList<int> IntList;	
	//TEST FOR LINKED LIST
	std::cout << "TEST FOR LINKED LIST" << std::endl;
	std::cout << "--------------------" << std::endl;
	{
		//TEST FOR PUSHFRONT() AND POPFRONT()
		int TestNum = 10;
		IntList.PushFront(TestNum);
		//Testing the value entered with last PushFront() with the first value in the list. Returns true if they match.
		Test.VerifyEqual(TestNum, IntList.GetFirstData(), "PushFront");
		IntList.PopFront();
		//Test if the front value is still 10. if not it returns true
		Test.VerifyNotEqual(TestNum, IntList.GetFirstData(), "PopFront");

		//TEST FOR PUSHBACK() AND POPBACK()
		TestNum = 20;
		IntList.PushBack(TestNum);
		//Testing the value entered with last PushBack() with the Last value in the list. returns true if they match.
		Test.VerifyEqual(TestNum, IntList.GetLastData(), "PushBack");
		IntList.PopBack();
		//Tests if the last value is still there. if not it returns true.
		Test.VerifyNotEqual(TestNum, IntList.GetLastData(), "PopBack");

		//TEST FOR ERASELIST AND SEARCH LIST
		TestNum = 30;
		IntList.EraseList();
		Test.VerifyListErase(IntList, "Erase List");
		IntList.PushBack(10);
		IntList.PushBack(20);
		IntList.PushBack(TestNum);
		//Test search, found data/key
		Test.VerifyEqual(true, IntList.Search(TestNum), "Search Match");
		//Test search, did NOT found data/key
		Test.VerifyNotEqual(true, IntList.Search(40), "Search NO match");

		//TEST FOR ERASE NODE
		TestNum = 40;
		IntList.EraseList();
		IntList.PushBack(10);		//adding node
		IntList.PushBack(20);		//adding node
		IntList.PushBack(30);		//adding node
		IntList.PushBack(TestNum);	//adding Test node

		IntList.EraseNode(TestNum);		//erasing node
		//if the search()-function returns a false then the node has been removed and the erase node test is passed.
		Test.VerifyEqual(false, IntList.Search(TestNum), "Erase Node");
		

		//TEST FOR SIZE OF LIST IN BYTES
		IntList.EraseList();
		IntList.PushBack(10);		//adding node
		IntList.PushBack(20);		//adding node
		//Each node takes 12 bytes so 2 nodes gives 24 bytes.
		Test.VerifyEqual(24, IntList.Size(), "Size in Bytes");
	}

	//Creating a Binary Search Tree for integers
	BST<int> Tree;
	//TEST FOR BINARY SEARCH TREE
	std::cout << std::endl << "TEST FOR BINARY SEARCH TREE" << std::endl;
	std::cout << "----------------------------" << std::endl;
	{
		//TEST INSERT / ADD LEAF
		int TestNum = 50;
		Tree.AddLeaf(TestNum);		//Add Leaf / Insert Node
		Test.VerifyEqual(true, Tree.Search(TestNum) ,"Insert");		

		//TEST SEARCH
		Tree.DeleteAll();
		TestNum = 25;
		Tree.AddLeaf(50);
		Tree.AddLeaf(75);
		Tree.AddLeaf(25);
		Tree.AddLeaf(15);
		Tree.AddLeaf(85);
		Tree.AddLeaf(95);
		Test.VerifyEqual(true, Tree.Search(TestNum), "Search");

		//TEST SIZE
		Tree.DeleteAll();
		Tree.AddLeaf(50);
		Tree.AddLeaf(75);
		Tree.AddLeaf(100);
		Tree.AddLeaf(15);
		//Size for each node is 12 Bytes so with 4 nodes it should be 48 bytes.
		Test.VerifyEqual(48, Tree.Size(), "Size");

		//TRAVERSAL TESTING
		
		Tree.DeleteAll();
	
		//Adding nodes to the tree
		Tree.AddLeaf(50);
		Tree.AddLeaf(25);
		Tree.AddLeaf(75);
		Tree.AddLeaf(15);
		Tree.AddLeaf(100);
		Tree.AddLeaf(85);
		Tree.AddLeaf(35);
		Tree.AddLeaf(65);

		//PRE-ORDER
		std::vector<int> TestVect;		//vector to store all nodes in PRE, IN or POST order

		//Adding same nodes data to the Test vector sorted in preorder
		TestVect.push_back(50);
		TestVect.push_back(25);
		TestVect.push_back(15);
		TestVect.push_back(35);
		TestVect.push_back(75);
		TestVect.push_back(65);
		TestVect.push_back(100);
		TestVect.push_back(85);

		Tree.PrintAndSave(Tree.PRE_ORDER);		//Prints to console and saves print order in a traversal vector
		std::cout << std::endl;
		for (int i = 0; i < TestVect.size(); i++)
		{
			std::cout << " ELEMENT: " << i << " ";
			Test.VerifyEqual(TestVect[i], Tree.TraversalVect[i], "PRE-ORDER");
		}
		
		//IN-ORDER
		TestVect.clear();			//Clears the test vector

		//Adding same nodes data to the Test vector sorted in TRAVERSAL IN-ORDER
		TestVect.push_back(15);
		TestVect.push_back(25);
		TestVect.push_back(35);
		TestVect.push_back(50);
		TestVect.push_back(65);
		TestVect.push_back(75);
		TestVect.push_back(85);
		TestVect.push_back(100);

		Tree.PrintAndSave(Tree.IN_ORDER);		//Prints to console and saves print order in a traversal vector
		std::cout << std::endl;
		for (int i = 0; i < TestVect.size(); i++)
		{
			std::cout << " ELEMENT: " << i << " ";
			Test.VerifyEqual(TestVect[i], Tree.TraversalVect[i], "IN-ORDER");
		}
		//POST-ORDER
		TestVect.clear();			//Clears the test vector

		//Adding same nodes data to the Test vector sorted in TRAVERSAL IN-ORDER
		TestVect.push_back(15);
		TestVect.push_back(35);
		TestVect.push_back(25);
		TestVect.push_back(65);
		TestVect.push_back(85);
		TestVect.push_back(100);
		TestVect.push_back(75);
		TestVect.push_back(50);

		Tree.PrintAndSave(Tree.POST_ORDER);		//Prints to console and saves print order in a traversal vector
		std::cout << std::endl;
		for (int i = 0; i < TestVect.size(); i++)
		{
			std::cout << " ELEMENT: " << i << " ";
			Test.VerifyEqual(TestVect[i], Tree.TraversalVect[i], "POST-ORDER");
		}

		//TEST ERASE / DELETE
		Tree.DeleteAll();	//to erase the whole list
		
		TestNum = 50;
		Tree.AddLeaf(TestNum);
		Tree.AddLeaf(75);
		Tree.AddLeaf(25);
		Tree.AddLeaf(15);
		Tree.AddLeaf(85);
		Tree.AddLeaf(95);
		Tree.AddLeaf(10);
		
		Tree.PrintAndSave(Tree.PRE_ORDER);	//LIST
		Tree.DeleteNode(TestNum);
		Tree.PrintAndSave(Tree.PRE_ORDER);		//Prints to console and saves print order in a traversal vector
		
		//ERASE ROOT WITH 2 CHILDREN
		std::cout << std::endl;
		std::cout <<"ERASE ROOT WITH TWO CHILDREN." <<std::endl;
		std::cout << "----------------------------" << std::endl;
		//if the return value of search() is false then the erase has succeeded.
		Test.VerifyEqual(false, Tree.Search(TestNum), "ERASE OF ROOT");
		Test.VerifyEqual(25, Tree.TraversalVect[0], "NEW ROOT MATCH");		//Check that the new root is there
		Test.VerifyEqual(15, Tree.TraversalVect[1], "LEFT CHILD INTACT");	
		Test.VerifyEqual(75, Tree.TraversalVect[3], "RIGHT CHILD INTACT");

		//ERASE NODE WITH LEFT CHILD
		Tree.DeleteNode(15);		//node 15 has a left child with value 10. see lines 188 - 196
		Tree.PrintAndSave(Tree.PRE_ORDER);
		std::cout << std::endl;
		std::cout << "ERASE NODE WITH LEFT CHILD." << std::endl;
		std::cout << "----------------------------"<< std::endl;
		Test.VerifyEqual(false, Tree.Search(15), "ERASE OF NODE");
		Test.VerifyEqual(10, Tree.TraversalVect[1], "NEW LEFT CHILD");

		//ERASE NODE WITH RIGHT CHILD
		Tree.DeleteNode(85);		//node 85 has a right child with value 95. see lines 188 - 196
		Tree.PrintAndSave(Tree.PRE_ORDER);
		std::cout << std::endl;
		std::cout << "ERASE NODE WITH RIGHT CHILD." << std::endl;
		std::cout << "----------------------------" << std::endl;
		Test.VerifyEqual(false, Tree.Search(85), "ERASE OF NODE");
		Test.VerifyEqual(95, Tree.TraversalVect[3], "NEW RIGHT CHILD");

		//ERASE NODE WITH NO CHILDREN
		Tree.DeleteNode(95);
		Tree.PrintAndSave(Tree.PRE_ORDER);
		std::cout << std::endl;
		std::cout << "ERASE NODE WITH NO CHILDREN." << std::endl;
		std::cout << "----------------------------" << std::endl;
		Test.VerifyEqual(false, Tree.Search(95), "ERASE OF NODE");
		
	}

	ConsolePause();
	return 0;
}