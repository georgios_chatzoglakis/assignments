// technique.hpp

#ifndef TECHNIQUE_HPP_INCLUDED
#define TECHNIQUE_HPP_INCLUDED

#include <helium/scene/lighting_property.hpp>
#include <helium/scene/texture_unit.hpp>
#include <helium/scene/shader_unit.hpp>

namespace helium
{
	namespace resource
	{
		class Material;
	}

	namespace scene
	{
		/*
		note(tommi): usually we implement something called Pass to 
		  let us handle more complex rendering in our technique,
		  but we keep it simple and skip it this time 
		*/
		class Technique
		{
		private: // non-copyable
			Technique(const Technique&);
			Technique& operator=(const Technique&);

		public:
			typedef std::unique_ptr<Technique> Ptr;

			static Ptr create();

		private:
			Technique();

		public:
			~Technique();

			bool operator==(const Technique& rhs);
			bool operator!=(const Technique& rhs);

			uint32_t get_id() const;

			bool has_lighting_property() const;
			void set_lighting_property(LightingProperty::Ptr&& lighting_property);
			const LightingProperty* get_lighting_property() const;

			bool has_shader_unit() const;
			void set_shader_unit(ShaderUnit::Ptr&& shader_unit);
			const ShaderUnit* get_shader_unit() const;

			bool has_textures() const;
			uint32_t get_texture_unit_count() const;
			void add_texture_unit(TextureUnit::Ptr&& texture_unit);
			const TextureUnit* get_texture_unit(uint32_t index) const;

			void set_depth_state(uint32_t state);
			uint32_t get_depth_state() const;

		private:
			static uint32_t ms_id;
			uint32_t m_id;

			LightingProperty::Ptr m_lighting_property;
			ShaderUnit::Ptr m_shader_unit;

			std::vector<TextureUnit::Ptr> m_texture_units;

			uint32_t m_depth;
		};
	}
}

#endif // TECHNIQUE_HPP_INCLUDED
