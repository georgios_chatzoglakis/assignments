// stdafx.h 

#pragma once

#include <SDKDDKVer.h>

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>
#include <cstdint>

#include <fstream>
#include <memory>
#include <list>
#include <array>
#include <vector>
#include <unordered_map>
#include <queue>

#include <config.hpp>
#include <Helium/debug.hpp>
#include <Helium/string.hpp>
#include <Helium/math.hpp>
#include <Helium/time.hpp>
#include <Helium/visitor.hpp>
#include <helium/mouse.hpp>
#include <helium/keyboard.hpp>
#include <helium/predef.hpp>
