// abstract_state.hpp

#ifndef ABSTRACTSTATE_HPP_INCLUDED
#define ABSTRACTSTATE_HPP_INCLUDED

namespace helium
{
	namespace gameplay
	{
		class AbstractState
		{
		public:
			virtual ~AbstractState();

			virtual void initialize() = 0;
			virtual void shutdown() = 0;
			virtual bool update() = 0;
			virtual void draw() = 0;
			virtual uint32_t next() = 0;

		protected:
			Time m_start_time;
			Time m_total_time;
			uint32_t m_id_hash;
		};
	}
}

#endif // ABSTRACTSTATE_HPP_INCLUDED
