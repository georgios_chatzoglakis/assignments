# README #

This repository is for my assignments in the course game programming III at Uppsala University, Campus Gotland.

These are:

* Assignment 1: Linked list, Binary search tree and unit testing for both of them.
* Assignment 2: a simple Webserver
* Assignment 3: 3D engine and 3D game

You can look at all the code and use it in your own projects if you like but please make a note that you borrowed the code from me if you do.

Links:

My blog:
http://gamertogamedev.blogspot.se/

Design doc: 
https://docs.google.com/document/d/18LdU_PB7cGeDt-UB6Zh1JVclKXCy-DDYhzRhvKBArHg/edit

Task tracker for the 3D game: 
https://trello.com/b/5sm63ssM/cuberacer


Best regards,

Georgios Chatzoglakis
chatzoglakis@gmail.com