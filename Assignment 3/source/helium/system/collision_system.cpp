// collision_system.cpp

#include "stdafx.h"
#include <helium/system/collision_system.hpp>

namespace helium
{
	namespace system
	{
		//static 
		CollisionSystem::Ptr CollisionSystem::create()
		{
			return CollisionSystem::Ptr(new CollisionSystem);
		}

		// private
		CollisionSystem::CollisionSystem()
		{
		}

		// public
		CollisionSystem::~CollisionSystem()
		{
		}

		void CollisionSystem::process()
		{
		}
	}
}
