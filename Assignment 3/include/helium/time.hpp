// time.hpp

#ifndef TIME_HPP_INCLUDED
#define TIME_HPP_INCLUDED

namespace helium
{
	class Time
	{
	public:
		static Time now();

	public:
		Time();
		Time(const Time& rhs);
		Time(const int64_t& rhs);

		void operator=(const int64_t& rhs);
		Time& operator=(const Time& rhs);			
		Time operator+(const Time& rhs);
		Time operator-(const Time& rhs);

		double as_seconds() const;
		double as_milliseconds() const;
		double as_microseconds() const;

	private:
		int64_t m_tick;
	};
}

#endif // TIME_HPP_INCLUDED
