// scene_node.cpp

#include "stdafx.h"
#include <helium/scene/scene_node.hpp>

namespace helium
{
	namespace scene
	{
		// static 
		SceneNode::Ptr SceneNode::create(Technique* technique)
		{
			return SceneNode::Ptr(new SceneNode(technique));
		}

		// private
		SceneNode::SceneNode(Technique* technique)
		{
			m_technique = technique;
		}

		// public
		SceneNode::~SceneNode()
		{
		}

		void SceneNode::set_position(const Vector3& position)
		{
			m_transform.set_position(position);
			m_volume.m_center = position;
		}

		void SceneNode::set_scale(const float scale)
		{
			m_transform.set_scale(scale);
		}

		void SceneNode::set_rotation(float pitch, float yaw, float roll)
		{
			m_transform.set_rotation(Vector3(pitch, yaw, roll));
		}

		const Vector3& SceneNode::get_position() const
		{
			return m_transform.get_position();
		}

		const Vector3& SceneNode::get_scale() const
		{
			return m_transform.get_scale();
		}

		const Vector3& SceneNode::get_rotation() const
		{
			return m_transform.get_rotation();
		}

		const Transform& SceneNode::get_transform() const
		{
			return m_transform;
		}

		void SceneNode::set_bounding_radius(float radius)
		{
			m_volume.m_radius = radius;
		}

		const BoundingSphere& SceneNode::get_bounding_volume() const
		{
			return m_volume;
		}

		void SceneNode::set_technique(Technique* technique)
		{
			m_technique = technique;
		}

		const Technique* SceneNode::get_technique() const
		{
			return m_technique;
		}
		
		void SceneNode::set_model(resource::Model* model)
		{
			m_model = model;
		}

		const resource::Model* SceneNode::get_model() const
		{
			return m_model;
		}

		// private
		void SceneNode::force_transform_recalc()
		{
			m_transform.recalculate();
		}
	}
}
