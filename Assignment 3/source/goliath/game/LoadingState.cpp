//LoadingState.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/resource/resource_cache.hpp>
#include <helium/system/render_system.hpp>
#include <helium/example/example_shared_resources.hpp>
#include <goliath/game/LoadingState.hpp>

#include <helium/resource/font.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>
#include <helium/resource/resource_creator.hpp>

namespace goliath
{
	namespace game
	{
		LoadingState::LoadingState()
		{
			m_font = nullptr;
		}

		LoadingState::~LoadingState()
		{
		}

		void LoadingState::initialize()
		{
			helium::resource::ResourceCache* rescache = helium::ServiceLocator<helium::resource::ResourceCache>::get_service();

			m_font = rescache->load_resource<helium::resource::Font>("font.fontdesc.txt", "example");

			helium::resource::Shader* shader = rescache->load_resource<helium::resource::Shader>("font.shader.txt", "example");
			helium::resource::ResourceCreator<helium::resource::Shader>::create(shader);

			helium::resource::VertexFormat* format = rescache->load_resource<helium::resource::VertexFormat>("font.vertexformat.txt", "example");
			helium::resource::ResourceCreator<helium::resource::VertexFormat>::create(format);
		}

		void LoadingState::shutdown()
		{
		}

		bool LoadingState::update()
		{
			return true;
		}

		void LoadingState::draw()
		{
		}

		uint32_t LoadingState::next()
		{
			return helium::String::hash32("examplescene", 12);
		}
	}
}
