//UDP ServerNetwork, low level network
//By Georgios Chatzoglakis

#ifndef SERVERNETWORK_HPP_INCLUDED
#define SERVERNETWORK_HPP_INCLUDED

#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")

#include <vector>


namespace goliath
{
	namespace network
	{
		class PacketManager;
		class ServerNetwork
		{
		private:
			struct Client
			{
				unsigned char s_ClientID;
				struct sockaddr_in s_Addr;
				SOCKET s_Sock;
				unsigned int s_Timestamp;
				bool s_IsAlive;
			};

		public:
			ServerNetwork(network::PacketManager* _PktMgr, const unsigned int& _MaxClients);
			~ServerNetwork();

			bool Initialize();
			void UpdateRecv();
			void SendAllInList();
			void Cleanup();
			/*returns true if all players are connected. else returns false*/
			//bool LookForConnections(const unsigned int& _numPlayers);
			void PrintClientAdress(Client* _Client);
			void SetNumRequestedClients(unsigned int _NumReqClients);

			unsigned int GetNumConnClients();

			bool ClientHasData(Client* client);
			bool IsAlive(Client* _Client);
			void RemoveClient(SOCKET& _ClientSock);

		private:
			void AddClient(SOCKET _ClientSock, struct sockaddr_in& _Addr, unsigned char _ClientID);
			/*Receive data from unknown*/
			bool ReceiveData();
			/*Receive data from Client*/
			bool ReceiveData(Client* _Client);
			void SendData(Client* _Client, const char* _DataBuff);
			bool ShutdownConnection(SOCKET& _ClientSock);
			void SetNumConnClients(unsigned int _NumConnClients);

		public:
			std::vector <Client*> m_Clients;

		private:
			SOCKET m_ServerSock;
			struct sockaddr_in m_ServerAddr;
			network::PacketManager* m_PktMgr;
			unsigned int m_MaxClients;
			unsigned int m_NumReqClients;
			unsigned int m_NumConnectedClients;
		};
	}
}
#endif // SERVERNETWORK_HPP_INCLUDED