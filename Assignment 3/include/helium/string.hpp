// string_.hpp

#ifndef STRINGUTILS_HPP_INCLUDED
#define STRINGUTILS_HPP_INCLUDED

#include <string>

namespace helium
{
	class String
	{
	public:
		static uint32_t hash32(const void* array, uint32_t length);
		static uint64_t hash64(const void* array, uint32_t length);
		static std::string format(const char* fmt, ...);
	};
}

#endif // STRINGUTILS_HPP_INCLUDED
