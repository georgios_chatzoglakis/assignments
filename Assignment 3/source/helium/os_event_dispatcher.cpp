// os_event_dispatcher.cpp

#include "stdafx.h"
#include <helium/os_event.hpp>
#include <helium/os_event_listener.hpp>
#include <helium/os_event_dispatcher.hpp>

namespace helium
{
	// static 
	OSEventDispatcher::Ptr OSEventDispatcher::create()
	{
		return OSEventDispatcher::Ptr(new OSEventDispatcher);
	}

	// private
	OSEventDispatcher::OSEventDispatcher()
	{
	}

	// public
	OSEventDispatcher::~OSEventDispatcher()
	{
		m_listener_list.clear();
	}

	void OSEventDispatcher::queue(OSEvent event)
	{
		m_event_queue.push(event);
	}

	void OSEventDispatcher::process()
	{
		while (!m_event_queue.empty())
		{
			OSEvent event = m_event_queue.front();
			m_event_queue.pop();

			auto it = m_listener_list.begin();
			while (it != m_listener_list.end())
			{
				(*it)->notify(&event);
				++it;
			}
		}
	}

	void OSEventDispatcher::attach(OSEventListener* listener)
	{
		m_listener_list.push_back(listener);
	}

	void OSEventDispatcher::detach(OSEventListener* listener)
	{
		auto it = m_listener_list.begin();
		while (it != m_listener_list.end())
		{
			if ((*it) == listener)
			{
				m_listener_list.erase(it);
				break;
			}
			++it;
		}
	}
}
