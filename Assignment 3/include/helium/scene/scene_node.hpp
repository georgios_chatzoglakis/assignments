// scene_node.hpp

#ifndef SCENENODE_HPP_INCELUDED
#define SCENENODE_HPP_INCELUDED

#include <helium/scene/transform.hpp>

namespace helium
{
	namespace resource
	{
		class Model;
	}

	namespace scene
	{
		class Technique;

		class SceneNode
		{
			friend class Scene;
		private: // non-copyable
			SceneNode(const SceneNode&);
			SceneNode& operator=(const SceneNode&);

		public:
			typedef std::unique_ptr<SceneNode> Ptr;

			static Ptr create(Technique* technique);

		private:
			SceneNode(Technique* technique);

		public:
			~SceneNode();

			void set_position(const Vector3& position);
			void set_scale(const float scale);
			void set_rotation(float pitch, float yaw, float roll);
			const Vector3& get_position() const;
			const Vector3& get_scale() const;
			const Vector3& get_rotation() const;
			const Transform& get_transform() const;

			void set_bounding_radius(float radius);
			const BoundingSphere& get_bounding_volume() const;

			void set_technique(Technique* technique);
			const Technique* get_technique() const;
			void set_model(resource::Model* model);
			const resource::Model* get_model() const;

		private:
			void force_transform_recalc();
			 
		private:
			BoundingSphere m_volume;
			Transform m_transform;
			Technique* m_technique;
			resource::Model* m_model;
		};
	}
}

#endif // SCENENODE_HPP_INCELUDED
