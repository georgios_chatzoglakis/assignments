//GameServer.cpp
//By Georgios Chatzoglakis

#include <goliath/server/GameServer.hpp>
#include <goliath/network/ServerNetwork.hpp>
#include <goliath/network/PacketManager.hpp>
#include <goliath/network/Packet.hpp>

#include <iostream>
namespace goliath
{
	namespace network
	{

		GameServer::GameServer()
		{
			m_CurrentPacket = nullptr;
			m_NumReqPlayers = 0;
			m_PktMgr = nullptr;
			m_Network = nullptr;
			m_IsRunning = false;
		}
		GameServer::~GameServer()
		{
			if (m_Network != nullptr)
			{
				m_Network->Cleanup();
				delete m_Network;
				m_Network = nullptr;
			}
			if (m_PktMgr != nullptr)
			{
				delete m_PktMgr;
				m_PktMgr = nullptr;
			}
		}
		bool GameServer::Initialize()
		{
			m_PktMgr = new PacketManager;
			m_Network = new ServerNetwork(m_PktMgr, MAX_PLAYERS);

			if (!m_Network->Initialize())
			{
				return false;
			}
			//Set the number of players. change this to input driven
			m_NumReqPlayers = 1;
			m_Network->SetNumRequestedClients(m_NumReqPlayers);
			return true;

		}
		bool GameServer::LookForConnections()
		{
			//OBS!!!
			//Create a timer of 30 seconds. all players connected in that time will be in the game

			//Also set a limit on when recvlist is updated with deltatime 15 times per second
			//and same for SendAll / sendlist

			//DO this in this loop and in Server.run()

			CheckPlayersAlive();

			while (m_Network->GetNumConnClients() != m_NumReqPlayers)
			{
				std::cout << "Looking for connections! " << std::endl;
				//update the recvlist(if new data is found)
				m_Network->UpdateRecv();

				ReadPacket();

				//Ping connected players from server? or does only clients ping?

				//add broadcast packet to update the state for everyone that is connected
				CreateServerStatePacket();

				//Send all packets in the sendlist
				m_Network->SendAllInList();
			}
			std::cout << "All Clients found, starting game! " << std::endl;
			m_IsRunning = true;
			return true;
		}
		bool GameServer::Run()
		{
			while (IsRunning())
			{
				CheckPlayersAlive();

				//updateRecvList. add deltatime so this is done only 15 times per second
				m_Network->UpdateRecv();

				ReadPacket();

				HandleGameLogic();
				//add broadcast packet to update the state for everyone that is connected
				CreateServerStatePacket();
				//Send all packets in the sendlist
				m_Network->SendAllInList();
			}
			return IsRunning();
		}
		void GameServer::Cleanup()
		{
			if (m_Network != nullptr)
			{
				m_Network->Cleanup();
				delete m_Network;
				m_Network = nullptr;
			}
		}
		bool GameServer::IsRunning()
		{
			return m_IsRunning;
		}
		void GameServer::ReadPacket()
		{
			m_CurrentPacket = nullptr;
			m_CurrentPacket = GetNewPacket();

			//if ActivePacket is not nullptr it means a packet is recv
			if (m_CurrentPacket != nullptr)
			{
				//Is the packet init type? 
				if (m_PktMgr->ReturnPacketType(m_CurrentPacket) == PACKET_TYPE_INIT)
				{
					char Data[1] = { '\0' };

					//Create a return packet
					m_PktMgr->CreateSendPacket(m_CurrentPacket->GetID(), PACKET_TYPE_INIT, Data, 1);
					std::cout << "Init Packet recieved from Player: " << m_CurrentPacket->GetID() << std::endl;
					//remove the packet from the recieved list
					m_PktMgr->RemoveFromRecvList();
				}
				//Is the packet ping type? then create a ping packet in return
				else if (m_PktMgr->ReturnPacketType(m_CurrentPacket) == PACKET_TYPE_PING)
				{
					char Data[1] = { '\0' };

					//Create a return packet
					m_PktMgr->CreateSendPacket(m_CurrentPacket->GetID(), PACKET_TYPE_PING, Data, 1);
					std::cout << "Ping Packet recieved from Player: " << m_CurrentPacket->GetID() << " and a new ping is sent." << std::endl;
					//remove the packet from the recieved list
					m_PktMgr->RemoveFromRecvList();
				}
			}
		}
		Packet* GameServer::GetNewPacket()
		{
			return m_PktMgr->GetNextRecvPacket();
		}
		void GameServer::CheckPlayersAlive()
		{
			for (unsigned int i = 0; i < m_Network->m_Clients.size(); i++)
			{
				if (m_Network->IsAlive(m_Network->m_Clients[i]) == false)
				{
					m_Network->RemoveClient(m_Network->m_Clients[i]->s_Sock);
				}
			}
		}
		void GameServer::CreateServerStatePacket()
		{
			if (m_Network->m_Clients.size() != 0)
			{
				char* DataBuffer = "State update string";
				m_PktMgr->CreateSendPacket(0, PACKET_TYPE_SERVER_STATE, DataBuffer, 1);
			}
		}
		void GameServer::HandleGameLogic()
		{

		}

	}
}
