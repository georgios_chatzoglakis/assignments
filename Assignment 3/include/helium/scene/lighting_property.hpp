// lighting_property.hpp

#ifndef LIGHTINGPROPERTY_HPP_INCLUDED
#define LIGHTINGPROPERTY_HPP_INCLUDED

namespace helium
{
	namespace scene
	{
		class LightingProperty
		{
		private: // non-copyable
			LightingProperty(const LightingProperty& rhs);
			LightingProperty& operator=(const LightingProperty& rhs);

		public:
			typedef std::unique_ptr<LightingProperty> Ptr;

			static Ptr create();

		private:
			LightingProperty();

		public:			
			~LightingProperty();

			const Vector3& get_ambient() const;
			const Vector3& get_diffuse() const;
			const Vector3& get_specular() const;
			const float get_specular_power() const;

			void set_ambient(const Vector3& ambient);
			void set_diffuse(const Vector3& diffuse);
			void set_specular(const Vector3& specular);
			void set_specular_power(float power);

		private:
			Vector3 m_ambient;
			Vector3 m_diffuse;
			Vector3 m_specular;
			float m_power;
		};
	}
}

#endif // LIGHTINGPROPERTY_HPP_INCLUDED
