//BST.h (BinarySearchTree)
//By Georgios Chatzoglakis

#pragma once

template <typename T>
class BST
{
private:
	struct node
	{
		T Key;
		node* Left;
		node* Right;
	};
public:
	enum Traversal
	{
		PRE_ORDER,
		IN_ORDER,
		POST_ORDER,
	};

	BST();

	~BST();
	//Insert/Adds a node with a key number to the tree.
	void AddLeaf(const T &_AddKey);
	//Delete/Erase the node with the specific key.
	void DeleteNode(const T &_DelKey);
	//Delete the whole tree
	void DeleteAll();
	//Search for entered Key and choose search traversal order: (PRE_ORDER, IN_ORDER or POST_ORDER)
	bool Search(const T &_SearchKey);
	//Returns the size of the Binary Search Tree in bytes.
	unsigned int Size();
	/*Prints to console and saves all data in traversal order into the traversal vector. 
	Choose traversal order: (PRE_ORDER, IN_ORDER or POST_ORDER)*/
	void PrintAndSave(Traversal _PrintOrder);

	//Used for storing the Data/Keys in a vector to be able to test them for traversal order
	std::vector<T> TraversalVect;

private:
	node * CreateLeaf(const T &_AddKey);
	void AddLeafPrivate(const T &_AddKey, node* _Node);
	void DeleteSubTree(node* _Node);
	void DeleteNodePrivate(node* _Parent, const T &_DelKey);
	void DeleteRootNode();
	void DeleteNodeMatch(node* _Parent, node* _Match, const bool &_Left);

	int FindBiggest();
	int FindBiggestRecursive(node* _Node);

	void SizePrivate(node* _Node, unsigned int &_Size);
	void SearchPrivate(node* _Node, const T &_SearchKey, bool &_Match);


	void TraversalPreOrder();		//depth first, skriv ut noden sedan dess children p� v�nster node f�rst. sedan g�r man upp och sedan ner till h�ger
	void TraversalPreOrder(node* _Node);

	void TraversalInOrder();		//depth first, l�gsta v�rdet och stigande till h�gsta. 1, 3, 5, 8 ,10
	void TraversalInOrder(node* _Node);


	void TraversalPostOrder();		//
	void TraversalPostOrder(node* _Node);


	node* m_Root;
};

//Exempel:
//template <typename T>
//void LinkedList<T>::AddLeaf()
//{
//
//}

template <typename T>
BST<T>::BST()
{
	m_Root = nullptr;
}

template <typename T>
BST<T>::~BST()
{
	void DeleteSubTree(node* m_Root);
	delete m_Root->Left;
	delete m_Root->Right;
	delete m_Root;
	m_Root = nullptr;
}

template <typename T>
void BST<T>::AddLeaf(const T &_AddKey)
{
	AddLeafPrivate(_AddKey, m_Root);
};

template <typename T>
void BST<T>::DeleteNode(const T &_DelKey)
{
	std::cout << std::endl << "Deleting node: " << _DelKey <<std::endl;
	DeleteNodePrivate(m_Root, _DelKey);
};

template <typename T>
void BST<T>::DeleteAll()
{
	DeleteSubTree(m_Root);
	m_Root = nullptr;
}

template <typename T>
bool BST<T>::Search(const T &_SearchKey)
{
	bool Match = false;
	SearchPrivate(m_Root, _SearchKey, Match);
	if(Match)
	{
		//std::cout << std::endl << "The Key: " << _SearchKey << " was found in the tree" << std::endl;
		return true;
	}
	else
	{
		//std::cout << std::endl << "The Key: " << _SearchKey << " was NOT found in the tree" << std::endl;
		return false;
	}
}

template <typename T>
void BST<T>::SearchPrivate(node* _Node, const T &_SearchKey, bool &_Match)		// search Preorder
{																					//hade problem med bool return type.
	if (_Node != nullptr)																//N�r match hittades s� returnerades true men eftersom det �r recursive
	{																				//s� blir return false  i loppen efter. bytte d� ut till att ta in reference parameter.
		//std::cout << _SearchKey << " : " << _Node->Key  << std::endl;
		if (_Node->Key == _SearchKey)
			_Match = true;
		SearchPrivate(_Node->Left, _SearchKey, _Match);
		SearchPrivate(_Node->Right, _SearchKey, _Match);
	}
}

template <typename T>
unsigned int BST<T>::Size()
{
	unsigned int ByteSize = 0;
	SizePrivate(m_Root, ByteSize);
	//std::cout << "Size in bytes: " << ByteSize << std::endl;
	return ByteSize;
}

template <typename T>
void BST<T>::SizePrivate(node* _Node, unsigned int &_ByteSize)
{
	if (_Node != nullptr)
	{
		SizePrivate(_Node->Left, _ByteSize);
		SizePrivate(_Node->Right, _ByteSize);
		_ByteSize += sizeof(struct node);
	}
}


template <typename T>
void BST<T>::PrintAndSave(Traversal _PrintOrder)
{
	TraversalVect.clear();				//Clears the traversal vector
	if (_PrintOrder == IN_ORDER)
		TraversalInOrder();
	else if (_PrintOrder == PRE_ORDER)
		TraversalPreOrder();
	else if (_PrintOrder == POST_ORDER)
		TraversalPostOrder();
	else
	{
		std::cout << std::endl << "No Valid traversal order entered so printing IN-ORDER traversal: " << std::endl;
		TraversalInOrder();
	}
}

template <typename T>
typename BST<T>::node * BST<T>::CreateLeaf(const T &_AddKey)
{
	node* NewNode = new node;
	NewNode->Key = _AddKey;
	NewNode->Left = nullptr;
	NewNode->Right = nullptr;
	return NewNode;
};

template <typename T>
void BST<T>::AddLeafPrivate(const T &_AddKey, node* _Node)
{
	if (m_Root == nullptr)				//If tree is empty create a root node
	{
		m_Root = CreateLeaf(_AddKey);
	}
	else if (_AddKey < _Node->Key)		//Is the key lower then the node key?
	{
		if (_Node->Left != nullptr)		//if the left node isn't a nullptr recursivly call the next node to the left
		{
			AddLeafPrivate(_AddKey, _Node->Left);		//GO LEFT
		}
		else
		{
			_Node->Left = CreateLeaf(_AddKey);
		}
	}
	else if (_AddKey > _Node->Key)		//If the key is a higher value then the node key.
	{
		if (_Node->Right != nullptr)		//if the right node isn't empty call the function again recursivly to go one step deeper to the right.
		{
			AddLeafPrivate(_AddKey, _Node->Right);		//GO RIGHT
		}
		else
		{
			_Node->Right = CreateLeaf(_AddKey);
		}
	}
	else
	{
		std::cout << "The key " << _AddKey << " has already been added to the tree." << std::endl;
	}
}

template <typename T>
void BST<T>::DeleteSubTree(node* _Node)
{
	if (_Node != nullptr)
	{
		if (_Node->Left != nullptr)
		{
			DeleteSubTree(_Node->Left);
		}
		if (_Node->Right != nullptr)
		{
			DeleteSubTree(_Node->Right);
		}
		//std::cout << "Deleting the node containing the key " << _Node->Key << std::endl;
		
		delete _Node;
		_Node = nullptr;
	}
};

template <typename T>
void BST<T>::DeleteNodePrivate(node* _Parent, const T &_DelKey)
{
	if (m_Root != nullptr)
	{

		if ((_DelKey < _Parent->Key) && (_Parent->Left != nullptr))		//LEFT SIDE
		{
			if (_Parent->Left->Key == _DelKey)
			{
				DeleteNodeMatch(_Parent, _Parent->Left, true);		//Match found on left side. set bool to true.
			}
			else
			{
				DeleteNodePrivate(_Parent->Left, _DelKey);		//Recursive call when no match was found.
			}
		}
		else if ((_DelKey > _Parent->Key) && (_Parent->Right != nullptr))	//RIGHT SIDE
		{
			if (_Parent->Right->Key == _DelKey)
			{
				DeleteNodeMatch(_Parent, _Parent->Right, false);	//Match found on right side, set bool to false.
			}
			else
			{
				DeleteNodePrivate(_Parent->Right, _DelKey);		//Recursive call when no match was found.
			}
		}
		else if (m_Root->Key == _DelKey)		// ROOT MATCH
		{
			DeleteRootNode();
		}
		else		// NO MATCH
		{
			std::cout << "The key " << _DelKey << " was not found" << std::endl;
		}
	}
	else
	{
		std::cout << "Nothing to delete. The Tree is empty" << std::endl;
	}
};

template <typename T>
void BST<T>::DeleteRootNode()
{
	if (m_Root != nullptr)
	{
		T RootKey = m_Root->Key;
		T BiggestInLeftSubtree;

		if ((m_Root->Left == nullptr) && (m_Root->Right == nullptr))
		{
			delete m_Root;
			m_Root = nullptr;
		}
		//if there is one child on Left side of the root
		else if ((m_Root->Left != nullptr) && (m_Root->Right == nullptr))
		{
			node* DelPtr = m_Root;
			m_Root = m_Root->Left;	//Moves the root to the left child.
			DelPtr->Left = nullptr;	//cut the link between the old root and the child (new root).
			delete DelPtr;
			DelPtr = nullptr;
			/*std::cout << "The root node with the key " << RootKey << " was deleted."
				<< "The new root has the key " << m_Root->Key << std::endl;*/

		}
		//if there is one child on Right side of the root
		else if ((m_Root->Left == nullptr) && (m_Root->Right != nullptr))
		{
			node* DelPtr = m_Root;
			m_Root = m_Root->Right;	//Moves the root to the right child.
			DelPtr->Right = nullptr;	//cut the link between the old root and the child (new root).
			delete DelPtr;
			DelPtr = nullptr;
			/*std::cout << "The root node with the key " << RootKey << " was deleted."
				<< "The new root has the key " << m_Root->Key << std::endl;*/
		}
		//if the root have two children
		else
		{
			BiggestInLeftSubtree = FindBiggestRecursive(m_Root->Left);		//Take the biggest value from the left subtree and save that
			DeleteNodePrivate(m_Root, BiggestInLeftSubtree);				//Delete the node containing the biggest number.
			m_Root->Key = BiggestInLeftSubtree;			//Overwrite the Root value of the match with the biggest value in the left subtree.
			/*std::cout << "The root key with value " << RootKey <<
				" was overwritten with key " << m_Root->Key << std::endl;*/
		}
	}
	else
	{
		std::cout << "Can not remove the root. The Tree is empty" << std::endl;
	}
};

template <typename T>
void BST<T>::DeleteNodeMatch(node* _Parent, node* _Match, const bool &_Left)
{
	if (m_Root != nullptr)			//just in case. this check has already been done in function call before this one.
	{
		T MatchKey = _Match->Key;
		T BiggestInLeftSubtree;

		//if matching node has 0 children
		if ((_Match->Left == nullptr) && (_Match->Right == nullptr))
		{
			if (_Left == true)		//If the match was on parents left side
			{
				_Parent->Left = nullptr; //set it to nullptr because the child will be deleted.
			}
			else
			{
				_Parent->Right = nullptr; //set it to nullptr because the child will be deleted.
			}
			delete _Match;
			_Match = nullptr;
			//std::cout << "The node containing " << MatchKey << " was deleted." << std::endl;
		}
		//if matching node has 1 child on left side
		else if (_Match->Left != nullptr && _Match->Right == nullptr)
		{
			if (_Left == true)		//If the match was on parents left side
			{
				_Parent->Left = _Match->Left;		//move the child of match to the parent left side.
			}
			else
			{
				_Parent->Right = _Match->Left;	//Move the child of the match to the parent right side.
			}
			_Match->Left = nullptr;
			delete _Match;
			_Match = nullptr;
			//std::cout << "The node containing " << MatchKey << " was deleted." << std::endl;
		}
		//if matching node has 1 child on right side
		else if (_Match->Left == nullptr && _Match->Right != nullptr)
		{
			if (_Left == true)		//If the match was on parents left side
			{
				_Parent->Left = _Match->Right;		//move the child of match to the parent left side.
			}
			else
			{
				_Parent->Right = _Match->Right;		//Move the child of the match to the parent right side.
			}
			_Match->Right = nullptr;
			delete _Match;
			_Match = nullptr;
			//std::cout << "The node containing " << MatchKey << " was deleted." << std::endl;
		}
		//if matching node has 2 children
		else
		{
			BiggestInLeftSubtree = FindBiggestRecursive(_Match->Left);		//Take the biggest value from the left subtree and save that
			DeleteNodePrivate(_Match, BiggestInLeftSubtree);				//Delete the node containing the biggest number.
			_Match->Key = BiggestInLeftSubtree;				//Overwrite the key value of the match with the biggest value in the left subtree.
		}
	}
	else
	{
		std::cout << "Tree is empty. No Match" << std::endl;
	}
};

template <typename T>
int BST<T>::FindBiggest()
{
	return FindBiggestRecursive(m_Root);
};

template <typename T>
int BST<T>::FindBiggestRecursive(node* _Node)
{
	if (m_Root == nullptr)
	{
		std::cout << "The tree is empty." << std::endl;
		return -1;
	}
	else
	{
		if (_Node->Right != nullptr)
		{
			return FindBiggestRecursive(_Node->Right);
		}
		else
		{
			return _Node->Key;
		}
	}
};

template <typename T>
void BST<T>::TraversalPreOrder()		//depth first, skriv ut noden sedan dess children p� v�nster node f�rst. sedan g�r man upp och sedan ner till h�ger
{
	std::cout << std::endl << std::endl  << "List in PRE-ORDER Traversal: " << std::endl;
	TraversalPreOrder(m_Root);
}

template <typename T>
void BST<T>::TraversalPreOrder(node* _Node)
{
	if (_Node != nullptr)
	{
		std::cout << _Node->Key << ", ";
		TraversalVect.push_back(_Node->Key);
		TraversalPreOrder(_Node->Left);
		TraversalPreOrder(_Node->Right);
	}
}

template <typename T>
void BST<T>::TraversalInOrder()		//depth first, l�gsta v�rdet och stigande till h�gsta. 1, 3, 5, 8 ,10
{
	std::cout << std::endl << std::endl << "List in IN-ORDER Traversal: " << std::endl;
	TraversalInOrder(m_Root);
}

template <typename T>
void BST<T>::TraversalInOrder(node* _Node)
{
	if (_Node != nullptr)
	{
		TraversalInOrder(_Node->Left);
		std::cout << _Node->Key << ", ";
		TraversalVect.push_back(_Node->Key);
		TraversalInOrder(_Node->Right);
		
	}
}

template <typename T>
void BST<T>::TraversalPostOrder()
{
	std::cout << std::endl << std::endl << "List in POST-ORDER Traversal: " << std::endl;
	TraversalPostOrder(m_Root);
}

template <typename T>
void BST<T>::TraversalPostOrder(node* _Node)
{
	if (_Node != nullptr)
	{
		TraversalPostOrder(_Node->Left);
		TraversalPostOrder(_Node->Right);
		std::cout << _Node->Key << ", ";
		TraversalVect.push_back(_Node->Key);
	}
}
