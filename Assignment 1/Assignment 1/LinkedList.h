//LinkedList.h
//This was first going to be a normal linked list but I changed it
//to a doublelinked list so I could have both pop_front and pop_back methods working correctly without looping through the whole list.
//By Georgios Chatzoglakis

#pragma once
template<typename T>
class LinkedList
{
private:
	struct node				//A node with one int and two pointers. using this to make a double linked list
	{						// working like this: nullptr <-(1)-> <-(2)-> <-(3)-> <-(4)-> nullptr
		T Data;
		node* Next;
		node* Prev;
	};

public:
	LinkedList();
	~LinkedList();

	//Add a new value to the beginning of the list.
	void PushFront(const T &_AddData);
	//Add a new value to the end of the list.
	void PushBack(const T &_AddData);
	//Delete the first element of the list.
	void PopFront();
	//Delete the last element of the list.
	void PopBack();
	//Delete the whole list.
	void EraseList();
	//Delete all the elements with the entered value.
	void EraseNode(const T &_DelData);
	//Search the list recursivly for the entered value.
	bool Search(const T &_SearchData);
	//Returns the size of the list in bytes.
	unsigned int Size();
	//Prints the current list to the console.
	void PrintList();

	node* GetFirst();
	T GetFirstData();
	node* GetLast();
	T GetLastData();

private:
	bool SearchPrivate(const node* _node, const T &_SearchData);
	void EraseNodePrivate(const node* _node, const T &_DelData);
	unsigned int SizePrivate(const node* _node);

	node* m_First;
	node* m_Last;
	node* m_Current;
	int m_NodeCounter;
};

template <typename T>
LinkedList<T>::LinkedList()
{
	m_First = nullptr;
	m_Last = nullptr;
	m_Current = nullptr;
	m_NodeCounter = 0;
};
template <typename T>
LinkedList<T>::~LinkedList()
{
	while (m_First != nullptr)
	{
		m_Current = m_First;
		m_First = m_First->Next;
		delete m_Current;
		m_Current = nullptr;
	}
};

template <typename T>
void LinkedList<T>::PushFront(const T &_AddData)
{
	node* NewNode = new node;
	NewNode->Next = nullptr;
	NewNode->Prev = nullptr;
	NewNode->Data = _AddData;

	if (m_First != nullptr)
	{
		NewNode->Next = m_First;	//Connects the "new" first node to the current first node
		m_First->Prev = NewNode;	//Connects the old first node with the new first node
		m_First = NewNode;			//Sets the new node as the first node in the list.
	}
	else
	{
		m_First = NewNode;			//Sets the new node as the head.
		m_Last = NewNode;			//and as a tail because there is only one node in the list.
	}
};
template <typename T>
void LinkedList<T>::PushBack(const T &_AddData)
{
	node* NewNode = new node;
	NewNode->Next = nullptr;
	NewNode->Prev = nullptr;
	NewNode->Data = _AddData;

	if (m_Last != nullptr)
	{
		NewNode->Prev = m_Last;		//Connects the "new" Last node to the current last node
		m_Last->Next = NewNode;		//Connects the old last node with the new last node.
		m_Last = NewNode;			//Sets the new node as the last node.
	}
	else
	{
		m_First = NewNode;		//Sets the new node as the head.
		m_Last = NewNode;		//and as a tail because there is only one node in the list.
	}
};

template <typename T>
void LinkedList<T>::PopFront()
{
	if (m_First != nullptr)
	{
		m_Current = m_First;			//make current point to the first node that will be deleted.
		if (m_Current->Next != nullptr)	//Go in here if the list is bigger then one node.
		{
			m_First = m_First->Next;		//Moves the first node one step to the next node in the list. (The "new" First)
			m_First->Prev = nullptr;		//sets the previous pointer to null because its now the first node.
		}
		else
		{
			m_First = nullptr;
			m_Last = nullptr;
			delete m_Current;				//Delete what used to be the first node.
			m_Current = nullptr;
		}
	}
	else
	{
		//std::cout << "The List is empty, nothing to delete." << std::endl;
	}
};

template <typename T>
void LinkedList<T>::PopBack()
{
	if (m_Last != nullptr)
	{
		m_Current = m_Last;
		if (m_Current->Prev != nullptr)
		{
			m_Current = m_Last;
			m_Last = m_Last->Prev;
			delete m_Current;
			m_Last->Next = nullptr;
		}
		else
		{
			m_First = nullptr;
			m_Last = nullptr;
			delete m_Current;				//Delete what used to be the last node.
			m_Current = nullptr;
		}
	}
	else
	{
		//std::cout << "The List is empty, nothing to delete." << std::endl;
	}
};

template <typename T>
void LinkedList<T>::EraseList()
{
	//std::cout << std::endl;
	while (m_First != nullptr)
	{
		m_Current = m_First;
		m_First = m_First->Next;
		//std::cout << m_Current->Data << " is being deleted." << std::endl;
		delete m_Current;
		m_Current = nullptr;
	}
	m_First = nullptr;
	m_Last = nullptr;
	//std::cout << std::endl << "The whole list is now deleted." << std::endl;
};

template <typename T>
void LinkedList<T>::EraseNode(const T &_DelData)
{
	m_NodeCounter = 0;
	EraseNodePrivate(m_First, _DelData);
};

template <typename T>
bool LinkedList<T>::Search(const T &_SearchData)
{
	return SearchPrivate(m_First, _SearchData);
};

template <typename T>
unsigned int LinkedList<T>::Size()
{
	return SizePrivate(m_First);
};

template <typename T>
void LinkedList<T>::PrintList()
{
	std::cout << std::endl << "This is the current list." << std::endl;
	std::cout << "-------------------------" << std::endl;
	m_Current = m_First;
	while (m_Current != nullptr)
	{
		std::cout << m_Current->Data << " ";
		m_Current = m_Current->Next;
	}
	std::cout << std::endl;
};

template <typename T>
bool LinkedList<T>::SearchPrivate(const node* _node, const T &_SearchData)
{
	if (_node == nullptr)	//if the list is empty or the search has passed through the whole list
	{
		if (m_NodeCounter > 0)
		{
			//std::cout << std::endl << "The value " << _SearchData << " was found " << m_NodeCounter << " times in the list" << std::endl;
			m_NodeCounter = 0;  //Resets the counter
			return true;
		}
		else
		{
			//std::cout << std::endl << "The value " << _SearchData << " was not found in the list" << std::endl;
			m_NodeCounter = 0;		//Resets the counter
			return false;
		}
	}
	else if (_node->Data == _SearchData)
	{
		//if value was found, add counter and search next node
		m_NodeCounter++;
		SearchPrivate(_node->Next, _SearchData);
	}
	else
	{
		//if value was NOT found, search next node.
		SearchPrivate(_node->Next, _SearchData);
	}
};

template <typename T>
void LinkedList<T>::EraseNodePrivate(const node* _node, const T &_DelData)
{
	if (_node != nullptr)
	{
		if ((_node == m_First) && (_node->Data == _DelData))		//Do if this IS the First node and _DelData match.
		{
			PopFront();								//_node is deleted
			m_NodeCounter++;
			EraseNodePrivate(m_First, _DelData);	//use the m_First pointer to send in the new first node of the list since _node is deleted
		}
		else if ((_node != m_First) && (_node != m_Last) && (_node->Data == _DelData))		//Do if this is not the First or last node.
		{
			m_Current = _node->Prev;		//Set m_Current to the previous node.
			m_Current->Next = _node->Next;	//reconnect the nodes so the previous node skips this node and points to the next one.

			m_Current = _node->Next;		//Set m_Current to the next node.
			m_Current->Prev = _node->Prev;	//Connect the backward connection of next node with the node before the one that will be deleted.
			delete _node;					//after the connections have been arranged this node can be deleted.
			_node = nullptr;
			m_NodeCounter++;				//To keep track of how many nodes get deleted.
			EraseNodePrivate(m_Current, _DelData);	//sends in the next node.
		}
		else if ((_node == m_Last) && (_node->Data == _DelData))		//Do if this is the last node and _DelData match
		{
			PopBack();						//_node is deleted
			m_NodeCounter++;
			EraseNodePrivate(m_Last->Next, _DelData); //Since _node was deleted send in the node after the last node. (in other words nullptr)
		}
		else
		{
			EraseNodePrivate(_node->Next, _DelData);	//nothing was found. go to the next node
		}
	}
	else if (m_NodeCounter > 0)
	{
		//std::cout << "The value " << _DelData << " was found and deleted " << m_NodeCounter << " times." << std::endl;
		m_NodeCounter = 0;
	}
	else
	{
		//std::cout << "The value " << _DelData << " was not in the list." << std::endl;
		m_NodeCounter = 0;
	}
};

template <typename T>
unsigned int LinkedList<T>::SizePrivate(const node* _node)
{
	if (_node == nullptr)
	{
		return 0;
	}
	return sizeof(struct node) + SizePrivate(_node->Next);
};

template <typename T>
typename LinkedList<T>::node* LinkedList<T>::GetFirst()
{
	return m_First;
}

template <typename T>
T LinkedList<T>::GetFirstData()
{
	if (m_First != nullptr)
		return m_First->Data;
	else
		return 0;
}

template <typename T>
typename LinkedList<T>::node* LinkedList<T>::GetLast()
{
	return m_Last;
}

template <typename T>
T LinkedList<T>::GetLastData()
{
	if (m_Last != nullptr)
		return m_Last->Data;
	else
		return 0;
}