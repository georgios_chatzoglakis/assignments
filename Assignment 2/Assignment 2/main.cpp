//Assignment 2, WebServer
//by Georgios Chatzoglakis

#include <iostream>
#include "Server.h"
//#include "vld.h"


//simplified pause function
void Pause()
{
	std::cout << "Press any key to continue";
	std::cin.get();
}

//main
int main(int argc, char* argv[])
{
	Server myServer;
	//Initialize Winsock.
	if (!myServer.InitializeWinsock())
	{
		Pause();
		return 0;
	}

	//Create a listening socket.
	SOCKET listenSocket = myServer.socketCreate();
	if (listenSocket == INVALID_SOCKET)
	{
		std::cout << "Error creating socket: " << WSAGetLastError();
		myServer.socketDestroy(listenSocket);
		myServer.CleanupWinsock();
		Pause();
		return 0;
	}
	//setup local address and port to be used by the server
	myServer.setupLocalAndPort();

	//Bind the listening socket.
	if (!myServer.bindSocket(listenSocket))
	{
		myServer.socketDestroy(listenSocket);
		myServer.CleanupWinsock();
		Pause();
		return 0;
	}

	//Activate listen for client sockets.
	if (!myServer.listenOnSocket(listenSocket))
	{
		myServer.socketDestroy(listenSocket);
		myServer.CleanupWinsock();
		Pause();
		return 0;
	}

	//main loop
	bool running = true;
	/*int i = 0;*/
	while (running)
	{
		//Accept a connection from a client.
		myServer.acceptClient(listenSocket);

		//Check connected clients
		myServer.UpdateClientConnections();


		Sleep(250);		//for testing purposes to slow things down
		/*i++;
		if (i > 30)
			break;*/
	}
	//Cleanup and closing sockets. they are not needed anymore.
	WSACleanup();
	myServer.socketDestroy(listenSocket);
	myServer.CleanupWinsock();
	Pause();
	return 0;
}
