//ServerNetwork.hpp

#include <goliath/network/ServerNetwork.hpp>
#include <goliath/network/PacketManager.hpp>
#include <goliath/network/Packet.hpp>

#include <iostream>

#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")
namespace goliath
{
	namespace network
	{
		ServerNetwork::ServerNetwork(PacketManager* _PktMgr, const unsigned int& _MaxClients)
		{
			m_NumConnectedClients = 9999;
			m_MaxClients = _MaxClients;
			m_PktMgr = _PktMgr;
			m_ServerSock = INVALID_SOCKET;
		}
		ServerNetwork::~ServerNetwork()
		{
			for (unsigned int i = 0; i < m_Clients.size(); i++)
			{
				closesocket(m_Clients[i]->s_Sock);
				delete m_Clients[i];
				m_Clients[i] = nullptr;
				m_Clients.erase(m_Clients.begin() + i);
				--i;
			}
			WSACleanup();
		}
		bool ServerNetwork::Initialize()
		{
			WSAData data;
			if (WSAStartup(MAKEWORD(2, 2), &data))
			{
				std::cout << "Failed to initialize Winsock" << std::endl;
				return false;
			}

			m_ServerSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
			if (m_ServerSock == INVALID_SOCKET)
			{
				std::cout << "Error creating socket: " << WSAGetLastError();
				closesocket(m_ServerSock);
				Cleanup();
				return false;
			}

			m_ServerAddr.sin_family = AF_INET;
			m_ServerAddr.sin_addr.S_un.S_addr = INADDR_ANY;
			m_ServerAddr.sin_port = htons(30000);
			memset(m_ServerAddr.sin_zero, 0, sizeof(m_ServerAddr.sin_zero));

			if (bind(m_ServerSock, (const sockaddr*)&m_ServerAddr, sizeof(m_ServerAddr)) < 0)
			{
				std::cout << "Could not bind socket" << std::endl;
				std::cout << "Error: " << WSAGetLastError() << std::endl;
				return false;
			}
			return true;
		}
		void ServerNetwork::UpdateRecv()
		{
			if (m_Clients.size() == m_NumConnectedClients)
			{
				//Check all clients
				for (unsigned int i = 0; i < m_Clients.size(); i++)
				{
					//if data exists
					while (ClientHasData(m_Clients[i]))
					{
						ReceiveData(m_Clients[i]);
					}
				}
			}
			else if (m_Clients.size() < m_MaxClients && m_Clients.size() < m_NumReqClients)
			{
				//Receive data from unknown sender
				ReceiveData();
			}
		}
		void ServerNetwork::SendAllInList()
		{
			while (m_PktMgr->GetNextSendPacket() != nullptr)
			{
				if (m_PktMgr->GetNextSendPacket()->GetID() == 0)
				{
					//Send same data to all clients
					for (unsigned int i = 0; i < m_Clients.size(); i++)
					{

						SendData(m_Clients[i], m_PktMgr->GetNextSendPacket()->GetPacketBuffer());
					}
					//Delete the packet from the list after its sent to all clients
					m_PktMgr->RemoveFromSendList();
				}
				else
				{
					int ID = m_PktMgr->GetNextSendPacket()->GetID();
					//Send Data to specific Client
					SendData(m_Clients[ID], m_PktMgr->GetNextSendPacket()->GetPacketBuffer());
					//Delete Packet from the list
					m_PktMgr->RemoveFromSendList();
				}
				//add "else if" for send reliable packets, packets can not be removed until answer is received
			}
		}
		void ServerNetwork::Cleanup()
		{
			for (unsigned int i = 0; i < m_Clients.size(); i++)
			{
				closesocket(m_Clients[i]->s_Sock);
				delete m_Clients[i];
				m_Clients[i] = nullptr;
				m_Clients.erase(m_Clients.begin() + i);
				--i;
			}
			WSACleanup();
		}
		void ServerNetwork::AddClient(SOCKET _ClientSock, struct sockaddr_in& _Addr, unsigned char _ClientID)
		{
			//Add a new client
			Client* client = new Client;
			client->s_ClientID = _ClientID;
			client->s_Sock = _ClientSock;
			client->s_Addr = _Addr;
			client->s_Timestamp = timeGetTime();
			client->s_IsAlive = true;

			m_Clients.push_back(client);
			std::cout << "New connection: ";
			PrintClientAdress(client);
		}
		bool ServerNetwork::ClientHasData(Client* _Client)
		{
			u_long Length = 0;
			ioctlsocket(_Client->s_Sock, FIONREAD, &Length);
			return Length > 0;
		}
		/*Receive data from unknown*/
		bool ServerNetwork::ReceiveData()
		{
			SOCKET ClientSock = INVALID_SOCKET;
			char Buffer[MAX_PACKET_SIZE];
			struct sockaddr_in Addr;
			Addr.sin_family = AF_INET;
			Addr.sin_addr.S_un.S_addr = INADDR_ANY;
			Addr.sin_port = htons(30000);
			memset(Addr.sin_zero, 0, sizeof(Addr.sin_zero));

			int AddrSize = sizeof(Addr);

			//Setting the socket to non blocking while checking for connections
			u_long nonBlock = 0;
			ioctlsocket(ClientSock, FIONREAD, &nonBlock);
			if (nonBlock > 0)
			{
				int RecvBytes = recvfrom(ClientSock, Buffer, sizeof(Buffer) - 1, 0, (struct sockaddr*)&Addr, &AddrSize);
				//int RecvBytes = recvfrom(ClientSock, Buffer, sizeof(Buffer), 0, (struct sockaddr*)&Addr, &AddrSize);
				Buffer[RecvBytes] = '\0';
				if (ClientSock == INVALID_SOCKET)
					/*if (bytes < 0)*/
				{
					std::cout << "Socket error when connecting: " << WSAGetLastError() << std::endl;
					return false;
				}
				if (RecvBytes > 0)
				{
					//check if the buffer contains an INIT packet.
					if (m_PktMgr->CheckDataIsInit(Buffer))
					{
						//Creating packet of recieved data and adding it to the recieved list
						if (m_PktMgr->CreateRecvPacket(Buffer, 0))
						{
							m_NumConnectedClients = static_cast<unsigned int>(m_Clients.size()) + 1;
							//Add client sock, client adress and a ClientID
							AddClient(ClientSock, Addr, static_cast<unsigned char>(m_NumConnectedClients));
							//Set the packet type to same as client ID so we know who its from
							m_PktMgr->GetLastRecvPacket()->SetID(static_cast<unsigned char>(m_NumConnectedClients));
							return true;
						}
					}
				}
			}
			return false;
		}
		/*Receive data from Client*/
		bool ServerNetwork::ReceiveData(Client* _Client)
		{
			char Buffer[MAX_PACKET_SIZE];
			int AddrSize = sizeof(_Client->s_Addr);

			//int RecvBytes = recvfrom(_Client->s_Sock, Buffer, sizeof(Buffer) - 1, 0, (struct sockaddr*)&_Client->s_Addr, &AddrSize);
			int RecvBytes = recvfrom(_Client->s_Sock, Buffer, sizeof(Buffer), 0, (struct sockaddr*)&_Client->s_Addr, &AddrSize);
			Buffer[RecvBytes] = '\0';
			if (_Client->s_Sock == INVALID_SOCKET)
			{
				std::cout << "Socket error when receiving: " << WSAGetLastError() << std::endl;
				return false;
			}
			if (RecvBytes > 0)
			{
				//Creating packet of recieved data and adding it to the recieved list
				if (m_PktMgr->CreateRecvPacket(Buffer, 0))
				{
					_Client->s_Timestamp = timeGetTime();
					return true;
				}
			}
			return false;
		}
		void ServerNetwork::SendData(Client* _Client, const char* _DataBuff)
		{
			int AddrSize = sizeof(_Client->s_Addr);
			int BytesSent = sendto(_Client->s_Sock, _DataBuff, sizeof(_DataBuff), 0, (struct sockaddr*)&_Client->s_Addr, AddrSize);
			if (BytesSent == SOCKET_ERROR)
			{
				std::cout << "Socket error when sending: " << WSAGetLastError() << std::endl;
			}
		}
		//void ServerNetwork::SendReliableData(Client* _Client, const char* _DataBuff)
		//{
		//
		//}
		bool ServerNetwork::IsAlive(Client* _Client)
		{
			unsigned int now = timeGetTime();
			if (_Client->s_IsAlive)
			{
				if (now - _Client->s_Timestamp > 3000)		//3 seconds, if more then set alive to false
				{
					_Client->s_IsAlive = false;
				}
			}
			return _Client->s_IsAlive;
		}
		void ServerNetwork::RemoveClient(SOCKET& _ClientSock)
		{
			ShutdownConnection(_ClientSock);
			closesocket(_ClientSock);
			for (unsigned int i = 0; i < m_Clients.size(); i++)
			{
				if (m_Clients[i]->s_Sock == _ClientSock)
				{
					delete m_Clients[i];
					m_Clients[i] = nullptr;
					m_Clients.erase(m_Clients.begin() + i);
					return;
				}
			}
			SetNumConnClients(m_NumConnectedClients - 1);
			std::cout << "Client was not sucessfully removed" << std::endl;
		}
		bool ServerNetwork::ShutdownConnection(SOCKET& _ClientSock)
		{
			if (shutdown(_ClientSock, SD_SEND) == SOCKET_ERROR)		//shuts down client connection.
			{
				std::cout << "Failed to shut down socket correctly, Error: " << WSAGetLastError();
				return false;
			}
			return true;
		}
		void ServerNetwork::PrintClientAdress(Client* _Client)
		{
			std::cout << (int)_Client->s_Addr.sin_addr.S_un.S_un_b.s_b1 << ".";
			std::cout << (int)_Client->s_Addr.sin_addr.S_un.S_un_b.s_b2 << ".";
			std::cout << (int)_Client->s_Addr.sin_addr.S_un.S_un_b.s_b3 << ".";
			std::cout << (int)_Client->s_Addr.sin_addr.S_un.S_un_b.s_b4 << std::endl;
		}
		void ServerNetwork::SetNumRequestedClients(unsigned int _NumReqClients)
		{
			m_NumReqClients = _NumReqClients;
		}
		void ServerNetwork::SetNumConnClients(unsigned int _NumConnClients)
		{
			m_NumConnectedClients = _NumConnClients;
		}
		unsigned int ServerNetwork::GetNumConnClients()
		{
			if (m_NumConnectedClients == 9999)
				return 0;
			else
				return m_NumConnectedClients;
		}
	}
}