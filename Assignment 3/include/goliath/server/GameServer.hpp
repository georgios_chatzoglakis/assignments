//GamerServer.hpp, high level network
//By Georgios Chatzoglakis

#ifndef GAMESERVER_HPP_INCLUDED
#define GAMESERVER_HPP_INCLUDED

namespace goliath
{
	namespace network
	{
#define MAX_PLAYERS 4

		class ServerNetwork;
		class PacketManager;
		class Packet;

		class GameServer
		{
		public:
			GameServer();
			~GameServer();

			bool Initialize();
			bool LookForConnections();
			bool Run();


			void HandleGameLogic();
			void Cleanup();

			bool IsRunning();

		private:
			Packet* GetNewPacket();
			void ReadPacket();
			void CreateServerStatePacket();
			void CheckPlayersAlive();

		private:
			PacketManager* m_PktMgr;
			ServerNetwork* m_Network;
			Packet* m_CurrentPacket;
			bool m_IsRunning;
			unsigned int m_NumReqPlayers;
		};
	}
}
#endif //GAMESERVER_HPP_INCLUDED