//game.cpp

#include "stdafx.h"
#include <helium/gameplay/state_factory.hpp>
#include <goliath/game/LoadingState.hpp>
#include <goliath/game/GameState.hpp>
#include <goliath/game/Game.hpp>

namespace goliath
{

	namespace game
	{
		//static 
		helium::gameplay::AbstractGame::Ptr Game::create()
		{
			return AbstractGame::Ptr(new Game);
		}


		// private
		Game::Game()
		{
		}

		// public
		Game::~Game()
		{
		}

		void Game::initialize()
		{
			AbstractGame::initialize();

			uint32_t string_hash[] =
			{
				helium::String::hash32("LoadingState", 12),
				helium::String::hash32("GameState", 9),
			};

			helium::gameplay::StateFactory* factory = m_state_manager->get_factory();
			factory->attach<LoadingState>(string_hash[0]);
			factory->attach<GameState>(string_hash[1]);
			m_state_manager->set_state(string_hash[0]);
		}

		void Game::shutdown()
		{
			AbstractGame::shutdown();
		}
	}
}
