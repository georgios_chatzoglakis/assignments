//Gamestate.hpp from example_scene_state.hpp

#ifndef GAMESTATE_HPP_INCLUDED
#define GAMESTATE_HPP_INCLUDED

#include <helium/gameplay/abstract_state.hpp>
#include <helium/scene/scene.hpp>

namespace goliath
{
	namespace game
	{
		class GameState : public helium::gameplay::AbstractState
		{
		public:
			GameState();
			~GameState();

			void initialize();
			void shutdown();
			bool update();
			void draw();
			uint32_t next();

		private:
			helium::scene::Scene::Ptr m_scene;
		};
	}
}

#endif // GAMESTATE_HPP_INCLUDED
