//PacketManager.cpp
//by Georgios Chatzoglakis


#include <goliath/network/PacketManager.hpp>
#include <goliath/network/Packet.hpp>

#include <Windows.h>
//#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")
namespace goliath
{
	namespace network
	{
		PacketManager::PacketManager()
		{
		}
		PacketManager::~PacketManager()
		{
			while (!m_RecvList.empty())
			{
				if (m_RecvList.front() != nullptr)
				{
					delete m_RecvList.front();
					m_RecvList.front() = nullptr;
				}
				m_RecvList.pop_front();
			}
			while (!m_SendList.empty())
			{
				if (m_SendList.front() != nullptr)
				{
					delete m_SendList.front();
					m_SendList.front() = nullptr;
				}
				m_SendList.pop_front();
			}
		}
		/*_Data = received data,
		_Flag(0 = incoming Packet)
		Returns a false if wrong flag is entered*/
		bool PacketManager::CreateRecvPacket(const char* _Data, const unsigned char _Flag)
		{
			if (_Flag == 0)
			{
				AddToRecvList(new Packet(_Data, _Flag));
				return true;
			}
			return false;
		}
		/*_Databuffer = data to send,
		_Flag(1 = outgoing Packet)
		Returns a false if wrong flag is entered*/
		bool PacketManager::CreateSendPacket(const unsigned char _PacketID, ePacketType _Type, const char* _Data, const unsigned char _Flag)
		{
			if (_Flag == 1)
			{
				const unsigned int Timestamp = timeGetTime();
				AddToSendList(new Packet(_PacketID, (unsigned char)_Type, Timestamp, _Data, _Flag));
				return true;
			}
			return false;
		}
		const unsigned char PacketManager::ReturnPacketType(Packet* _ActivePacket)
		{
			return _ActivePacket->GetType();
		}
		bool PacketManager::CheckDataIsInit(const char* _Data)
		{
			Packet TempPack = Packet(_Data, 0);
			if (TempPack.GetPacketHeader()->s_Type == PACKET_TYPE_INIT)
			{
				return true;
			}
			return false;
		}
		/*Returns nullptr if the list is empty*/
		Packet* PacketManager::GetNextRecvPacket()
		{
			if (m_RecvList.size() == 0)
				return nullptr;
			return m_RecvList.front();
		}
		Packet* PacketManager::GetLastRecvPacket()
		{
			if (m_RecvList.size() == 0)
				return nullptr;
			return m_RecvList.back();
		}
		/*Returns nullptr if the list is empty*/
		Packet* PacketManager::GetNextSendPacket()
		{
			if (m_SendList.size() == 0)
				return nullptr;
			return m_SendList.front();
		}
		void PacketManager::AddToRecvList(Packet* _NewPacket)
		{
			m_RecvList.push_back(_NewPacket);
		}
		void PacketManager::AddToSendList(Packet* _NewPacket)
		{
			m_SendList.push_back(_NewPacket);
		}
		/*Returns true if packet was sucessfully removed*/
		bool PacketManager::RemoveFromRecvList()
		{
			if (m_RecvList.size() != 0)
			{
				delete m_RecvList.front();
				m_RecvList.front() = nullptr;
				m_RecvList.pop_front();
				return true;
			}
			return false;
		}
		/*Returns true if packet was sucessfully removed*/
		bool PacketManager::RemoveFromSendList()
		{
			if (m_SendList.size() != 0)
			{
				delete m_SendList.front();
				m_SendList.front() = nullptr;
				m_SendList.pop_front();
				return true;
			}
			return false;
		}
	}
}