//Server.h

#pragma once

#include <winsock2.h>
#include <Windows.h>
#include <ws2tcpip.h>
#include <vector>
#include <string>

#pragma comment(lib, "ws2_32.lib")

#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")


class Server
{
private:
	struct Client
	{
		struct sockaddr_in s_Addr;
		SOCKET s_Sock;
		unsigned int s_Timestamp;
		unsigned int s_Tick;
		bool KeepAlive;
	};
	struct Header
	{
		//Header info
		std::string s_HttpStatus;
		std::string s_ContentType;
		std::string s_CharSet;
		int s_ContentLength;
		std::string s_Connection;
		std::string s_SeverName;
	};
public:
	Server();
	~Server();
	bool InitializeWinsock();
	void CleanupWinsock();

	SOCKET socketCreate();
	void socketDestroy(SOCKET& _Sock);

	void setupLocalAndPort();

	bool bindSocket(SOCKET& _ListenSock);
	bool listenOnSocket(SOCKET& _ListenSock);

	bool acceptClient(SOCKET _ListenSock);
	void addClient(SOCKET _ClientSock, struct sockaddr_in& _Addr);
	
	void UpdateClientConnections();
	void clientUpdateTimestamp(Client* _Client);
	bool clientCheckAlive(Client* _Client, unsigned int& _Pos);

	int clientHasData(Client* _Client);
	int serverHandleData(Client* _Client);
	bool serverSendData(Client* _Client, const std::string _Type);
	
	void printClientAddress(struct sockaddr_in* _Addr);

	void removeClient(unsigned int& _Pos);
	bool shutdownConnection(SOCKET& _ClientSock);
	bool loadWebPage(const std::string& _WebAddr);
	char* loadImage(const std::string& _ImgName);
	void setHeaderInfo();
	void updateHeader();
	
private:
	WSAData m_Data;
	struct sockaddr_in m_Local;
	char m_RecvBuff[4096];
	int m_BuffLength;
	std::vector<Client*> ClientList;
	Header m_Headerinfo;
	std::string m_HeaderStr;
	char *m_OutBuffer;
	std::string m_Html;
	char* m_ImageData;
	unsigned int m_ImageLength;
};