// resource.cpp

#include "stdafx.h"
#include <helium/resource/resource.hpp>

namespace helium
{
	namespace resource
	{
		Resource::Resource()
		{
			m_id = (uint32_t)-1;
		}

		// virtual
		Resource::~Resource()
		{
		}

		void Resource::set_id(uint32_t id)
		{
			m_id = id;
		}

		uint32_t Resource::get_id() const
		{
			return m_id;
		}
	}
}
